# README #

### What is this repository for? ###

Here you find Version 2 of the MS Windows Application SondePredictor.
SondePredictor tries to calculate the landing position of radio sondes that are carried by ballons, started from DWD weather service or other institutions.

### How do I get set up? ###

To install SondePredictor, download Installation.zip (see "Download" area) and copy all its content into a directory.

Also install SondeMonitor. That program outputs a groundtrack file that SondePredictor reads.

SondePredictor (short: SP) needs some parameters to work. Give them as start parameters.

Example: SondePredictor.exe "com1" c:\SondeMonitor\Data

SP will currently send the estimated landing position to com1 (or any other COM port you choose) in NMEA format.
The given directory is SPs working directory.

It is planned to replace these start parameters by an INI file to support further settings.

To run SP also start SondeMonitor.

### What about programming environment? ###

SP here is a QT project, hosted at Bitbucket as GIT repository, so:

Get GIT from [git-for-windows.github.io](https://git-for-windows.github.io/)

Get TortoiseGit from [tortoisegit.org](https://tortoisegit.org/)

Get Qt and Qt Creatorfrom [www.qt.io](http://www.qt.io/developers/)

Clone the SondePredictor repository, see menu: Actions - Clone

Please set QtCreator tab stop width to 4, using tab character instead of blancs

### Who do I talk to? ###

For questions contact repository owner "Asohen" Ulrich Schulz.