#ifndef Configuration_H
#define Configuration_H

#include <QObject>
#include <QString>


class Configuration : public QObject
{
	Q_OBJECT

	// constants ----------------------------------------------------------------------------------
	public:
		static const QString		CONFIGURATION_FILE_NAME;
	private:
		static const QString		DEFAULT_DIRECTORY_NAME;
		static const QString		DEFAULT_GPS_NMEA_INPUT_NAME;
		static const QString		DEFAULT_PREDICTION_NMEA_OUTPUT_NAME;
		static const uint			DEFAULT_DETAIL;
		static const QString		GROUP_FILES;
		static const QString		KEY_DIRECTORY;
		static const QString		KEY_GPS_INPUT;
		static const QString		KEY_PREDICTION_OUTPUT;
		static const QString		KEY_DETAIL;

	// variables ----------------------------------------------------------------------------------
	public:
		struct
		{
			QString	DirectoryName;					// name of directory for input and output files
			QString	GpsNmeaInputName;				// name of GPS NMEA input COM interface (e.g. COM4)
			QString	PredictionNmeaOutputName;		// name of prediction NMEA output COM interface (e.g. COM5)
			uint	Detail;					// verbosity for detail output file
		} Parameter;

	private:

	// functions ----------------------------------------------------------------------------------
	public:
		Configuration(QObject* parent = 0);
		virtual ~Configuration();

		void HandleDefaultFile();		// if confiuration file doesn't exist, create one with defaults
		void ReadParameters();			// read confiuration file
};

#endif
