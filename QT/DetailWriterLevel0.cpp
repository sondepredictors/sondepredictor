#include "Common.h"
#include "DetailWriterLevel0.h"


//-------------------------------------------------------------------------------------------------
DetailWriterLevel0::DetailWriterLevel0
	(
		CsvWriter&									csv,
		unsigned long								csvMode,
		BlockFilterDetail::BlockListLevel0T&		blockList,
		BlockFilterDetail::TimeAverageT&			timeAverage,
		BlockFilterDetail::EquationsLevel0T&		equations,
		BlockFilterDetail::MaxDeviationT&			maxTimeDeviation,
		BlockFilterDetail::MaxDeviationsLevel0T&	maxDeviations,
		Position&									interpolation
	)
	: Csv(csv)
	, CsvMode(csvMode)
	, BlockList(blockList)
	, TimeAverage(timeAverage)
	, Equations(equations)
	, MaxTimeDeviation(maxTimeDeviation)
	, MaxDeviations(maxDeviations)
	, Interpolation(interpolation)
{
}

//-------------------------------------------------------------------------------------------------
DetailWriterLevel0::~DetailWriterLevel0()
{
}

//-------------------------------------------------------------------------------------------------
void DetailWriterLevel0::InitTimeStatistic()
{
	TimeStatistic.Init();
}

//-------------------------------------------------------------------------------------------------
void DetailWriterLevel0::InitStatistic()
{
	Statistic.Init();
}

//-------------------------------------------------------------------------------------------------
void DetailWriterLevel0::CollectTimeStatistic1(long startIndex, long count)
{
	if(CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_0)
	{
		long endIndex = startIndex + count - 1;
		if(endIndex >= 0)
		{
			endIndex = startIndex + count - 1;
		}
		else
		{
			endIndex = 0;
		}
		if(startIndex	< TimeStatistic.Index.Min)	{ TimeStatistic.Index.Min = startIndex; }
		if(endIndex		> TimeStatistic.Index.Max)	{ TimeStatistic.Index.Max = endIndex; }

		if(TimeAverage.Enabled)
		{
			TimeStatistic.TimeAverage.SetMinMax(TimeAverage.Value);
			TimeStatistic.TimeAverageCount ++;
		}

		if(MaxTimeDeviation.Enabled)
		{
			TimeStatistic.MaxTimeDeviation		.SetMinMax(MaxTimeDeviation.Value);
			TimeStatistic.MaxTimeDeviationIndex	.SetMinMax(MaxTimeDeviation.Index);
			TimeStatistic.MaxTimeDeviationCount ++;

			if(MaxTimeDeviation.Index < BlockList.size())
			{
				for(long i = startIndex; i < startIndex + count; i++)
				{
					if(!BlockList[i].Enabled)
					{
						TimeStatistic.DisabledBlockIndex.SetMinMax(i);
					}
				}
			}
			else
			{
				QTextStream(stdout) << "DetailWriterLevel0::WriteDetail1: index=" << MaxTimeDeviation.Index << " size=" << BlockList.size() << endl;
				QTextStream(stdout).flush();
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
void DetailWriterLevel0::CollectTimeStatistic2(long startIndex, long count)
{
	if(CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_0)
	{
		for(long i = startIndex; i < startIndex + count; i++)
		{
			if(!BlockList[i].Enabled)
			{
				TimeStatistic.DisabledBlockCount ++;
			}
		}

		long completedIndex = startIndex - 1;
		if(completedIndex >= 0)
		{
			TimeStatistic.DisableCountIndex	= completedIndex;
			TimeStatistic.DisableCount		= BlockList[completedIndex].DisableCount;
		}
	}
}

//-------------------------------------------------------------------------------------------------
void DetailWriterLevel0::CollectStatistic1(long startIndex, long count)
{
	if(CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_0)
	{
		long endIndex = startIndex + count - 1;
		if(endIndex >= 0)
		{
			endIndex = startIndex + count - 1;
		}
		else
		{
			endIndex = 0;
		}
		if(startIndex	< Statistic.Index.Min)	{ Statistic.Index.Min = startIndex; }
		if(endIndex		> Statistic.Index.Max)	{ Statistic.Index.Max = endIndex; }

		if(Equations.Valid)
		{
#ifdef USE_LINEAR
			Statistic.Altitude_xm	.SetMinMax(Equations.Altitude.xm);
			Statistic.Altitude_ym	.SetMinMax(Equations.Altitude.ym);
			Statistic.Altitude_xv	.SetMinMax(Equations.Altitude.xv);
			Statistic.Altitude_kv	.SetMinMax(Equations.Altitude.kv);
			Statistic.Altitude_a	.SetMinMax(Equations.Altitude.a);
			Statistic.Altitude_b	.SetMinMax(Equations.Altitude.b);
#else
			Statistic.Altitude_x	.SetMinMax(Equations.Altitude.x);
			Statistic.Altitude_x2	.SetMinMax(Equations.Altitude.x2);
			Statistic.Altitude_x3	.SetMinMax(Equations.Altitude.x3);
			Statistic.Altitude_x4	.SetMinMax(Equations.Altitude.x4);
			Statistic.Altitude_y	.SetMinMax(Equations.Altitude.y);
			Statistic.Altitude_yx2	.SetMinMax(Equations.Altitude.yx2);
			Statistic.Altitude_xy	.SetMinMax(Equations.Altitude.xy);
			Statistic.Altitude_a	.SetMinMax(Equations.Altitude.a);
			Statistic.Altitude_b	.SetMinMax(Equations.Altitude.b);
			Statistic.Altitude_c	.SetMinMax(Equations.Altitude.c);
#endif
			Statistic.LastTime		.SetMinMax(Equations.LastTime);

			for(long i = startIndex; i < startIndex + count; i++)
			{
				if(BlockList[i].Enabled && BlockList[i].Deviation.Valid)
				{
					Statistic.Altitude_Deviation.SetMinMax(BlockList[i].Deviation.Altitude);
				}
			}

			if(MaxDeviations.Altitude.Enabled)
			{
				Statistic.Altitude_MaxDeviationIndex	.SetMinMax(MaxDeviations.Altitude.Index);
				Statistic.Altitude_MaxDeviation			.SetMinMax(MaxDeviations.Altitude.Value);
			}

			Statistic.EquationCount ++;
		}
	}
}

//-------------------------------------------------------------------------------------------------
void DetailWriterLevel0::WriteDetail(unsigned long startIndex, unsigned long count, long lineNumber,
							   long disableCount, long maxLoopCount
							   )
{
	Csv.EndLine();

	if(		CsvMode == BlockFilterDetail::DETAIL_MODE_BASIC
		||	CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_0
		)
	{
		Csv.AddElement(lineNumber);											// Line
		Csv.AddElement((long)BlockList.size() - 1);							// BlkI

		long lastIndex = startIndex + count - 1;
		if(		lastIndex >= 0
			&&	lastIndex == BlockList.size() - 1
			)
		{
			Csv.AddElement(BlockList[lastIndex].Block.RelativeTime);		// Time
			Csv.AddElement(BlockList[lastIndex].Block.Altitude);			// Alt
			if(CsvMode == BlockFilterDetail::DETAIL_MODE_BASIC)
			{
				Csv.AddElement(BlockList[lastIndex].Block.Latitude);		// Lat
				Csv.AddElement(BlockList[lastIndex].Block.Longitude);		// Lon
			}
		}
		else
		{
			Csv.AddElement("-");
			Csv.AddElement("-");
			if(CsvMode == BlockFilterDetail::DETAIL_MODE_BASIC)
			{
				Csv.AddElement("-");
				Csv.AddElement("-");
			}
			QTextStream(stdout) << "DetailWriterLevel0::WriteDetail: lastIndex=" << lastIndex
								<< " BlockList.size()=" << BlockList.size() << endl;
			QTextStream(stdout).flush();
		}

		if(CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_0)
		{
			Csv.AddElement(TimeStatistic.Index);
			Csv.AddElement(TimeStatistic.TimeAverageCount);
			Csv.AddElement(TimeStatistic.TimeAverage);
			Csv.AddElement(TimeStatistic.MaxTimeDeviationCount);
			Csv.AddElement(TimeStatistic.MaxTimeDeviationIndex);
			Csv.AddElement(TimeStatistic.MaxTimeDeviation);
			Csv.AddElement(TimeStatistic.DisabledBlockCount);
			Csv.AddElement(TimeStatistic.DisabledBlockIndex);
			Csv.AddElement(TimeStatistic.DisableCountIndex);
			Csv.AddElement(TimeStatistic.DisableCount);

			Csv.AddElement(Statistic.Index);
			Csv.AddElement(Statistic.EquationCount);
			Csv.AddElement(Statistic.LastTime);
#ifdef USE_LINEAR
			Csv.AddElement(Statistic.Altitude_xm);
			Csv.AddElement(Statistic.Altitude_ym);
			Csv.AddElement(Statistic.Altitude_xv);
			Csv.AddElement(Statistic.Altitude_kv);
			Csv.AddElement(Statistic.Altitude_a);
			Csv.AddElement(Statistic.Altitude_b);
#else
			Csv.AddElement(Statistic.Altitude_x);
			Csv.AddElement(Statistic.Altitude_x2);
			Csv.AddElement(Statistic.Altitude_x3);
			Csv.AddElement(Statistic.Altitude_x4);
			Csv.AddElement(Statistic.Altitude_y);
			Csv.AddElement(Statistic.Altitude_yx2);
			Csv.AddElement(Statistic.Altitude_xy);
			Csv.AddElement(Statistic.Altitude_a);
			Csv.AddElement(Statistic.Altitude_b);
			Csv.AddElement(Statistic.Altitude_c);
#endif

			Csv.AddElement(Statistic.Altitude_Deviation);
			Csv.AddElement(Statistic.Altitude_MaxDeviationIndex);
			Csv.AddElement(Statistic.Altitude_MaxDeviation);

			Csv.AddElement(disableCount);
			Csv.AddElement(maxLoopCount);

			if(Equations.Valid)
			{
#ifdef USE_LINEAR
				Csv.AddElement(Equations.Altitude.xm);
				Csv.AddElement(Equations.Altitude.ym);
				Csv.AddElement(Equations.Altitude.xv);
				Csv.AddElement(Equations.Altitude.kv);
				Csv.AddElement(Equations.Altitude.a);
				Csv.AddElement(Equations.Altitude.b);
#else
				Csv.AddElement(Equations.Altitude.x);
				Csv.AddElement(Equations.Altitude.x2);
				Csv.AddElement(Equations.Altitude.x3);
				Csv.AddElement(Equations.Altitude.x4);
				Csv.AddElement(Equations.Altitude.y);
				Csv.AddElement(Equations.Altitude.yx2);
				Csv.AddElement(Equations.Altitude.xy);
				Csv.AddElement(Equations.Altitude.a);
				Csv.AddElement(Equations.Altitude.b);
				Csv.AddElement(Equations.Altitude.c);
#endif
			}
			else
			{
#ifdef USE_LINEAR
				for(int i = 0; i < 6; i++)
#else
				for(int i = 0; i < 7; i++)
#endif
				{
					Csv.AddElement("");
				}
			}
		}

		if(Equations.Valid)		{ Csv.AddElement(Equations.LastTime);		} else { Csv.AddElement(""); }	// IpoT
		if(Interpolation.Valid)	{ Csv.AddElement(Interpolation.Altitude);	} else { Csv.AddElement(""); }	// IpoAlt

		if(CsvMode == BlockFilterDetail::DETAIL_MODE_BASIC)
		{
			if(Interpolation.Valid)	{ Csv.AddElement(Interpolation.Latitude);	} else { Csv.AddElement(""); }	// IpoLat
			if(Interpolation.Valid)	{ Csv.AddElement(Interpolation.Longitude);	} else { Csv.AddElement(""); }	// IpoLon
		}
	}
}

//-------------------------------------------------------------------------------------------------
void DetailWriterLevel0::SkipElement(unsigned long count)
{
	for(unsigned long i = 0; i < count; i++)
	{
		Csv.AddElement("");
	}
}
