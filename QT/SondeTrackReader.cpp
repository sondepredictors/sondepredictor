#include <windows.h>										// Sleep
#include <limits>
#include <cmath>

#include "Common.h"
#include "SondeTrackReader.h"


const double	SondeTrackReader::SECONDS_PER_LINE	= 1.;


//-------------------------------------------------------------------------------------------------
SondeTrackReader::SondeTrackReader
	(
		GroundTrackFormat::Format	inputFileFormat,
		QFile&				inputFile,
#ifdef USE_BLOCK_FILTER
		CsvWriter&			csv,
		unsigned long		csvMode,
#endif
		QTextStream&		resultStream
	)
	:	InputFile(inputFile),
		InputFileInfo(inputFile.fileName()),
		InputFileFormat(inputFileFormat),
		GroundTrackFileFormat(inputFileFormat),
		ResultStream(resultStream),
#ifdef USE_BLOCK_FILTER
		FilteredBlock		(csv, csvMode),
		FilteredVelo		(csv, csvMode, FilteredBlock),
		FilteredVeloRatio	(csv, csvMode, FilteredBlock)
#else
		FilteredAltVelo(Parameter::ALT_VELO_FILTER)
#endif
{
	CurrentPhase			= Position::PHASE_STARTUP;
	ActualBurstAltitude		= -std::numeric_limits<double>::max();		// initialize for maximum calculation
	WaitingTime				= 0;
#ifdef USE_BLOCK_FILTER
	PhaseChangeTime			= 0;
#else
	DescentEvent			= false;
	PhaseChangeAltitude		= 0;
#endif
	EndOfData				= false;

	PhaseChange				= false;

	NewAltitude				= 0;
	Altitude				= 0;
	LastTime				= 0;
	LastAltitude			= 0;
	LastLatitude			= 0;
	LastLongitude			= 0;
	LastAltVelo				= 0;
	CompareTime				= 0;
	CompareAlt				= 0;
	CompareLat				= 0;
	CompareLon				= 0;
	CompareAltVelo			= 0;
	StartUpCompareCount		= 0;

	InputFilePosition		= 0;
	StartTime				= Parameter::MIN_TIME;
	LineTime				= 0.;
	FirstLineEvaluated		= false;

	InputFileInfo.setCaching(false);
}

//-------------------------------------------------------------------------------------------------
bool SondeTrackReader::NewCurrentPosition(Position& position)
{
	bool result = false;

	InputFile.open(QIODevice::ReadOnly);
	InputFile.seek(InputFilePosition);

	bool available = true;
	while(available)
	{
		char	character;									// one character to be read
		qint64	count = InputFile.read(&character, sizeof(char));

		switch(count)
		{
			case 0:											// currently no new position available
			{
//				QTextStream(stdout) << "    Waiting for next ground track position" << endl;
//				QTextStream(stdout).flush();
				available = false;
				EndOfData = true;
				break;
			}

			case -1:										// error
				available = false;
				EndOfData = true;
				break;

			default:										// more text available
			{
				EndOfData = false;
				if(character == '\n')						// end of line
				{
					result = EvaluateLine(position);
					Text.clear();
					LineTime += SECONDS_PER_LINE;
					available = false;						// return with completed line
				}
				else										// line nor completed yet
				{
					Text.append(character);
				}
				InputFilePosition ++;
				break;
			}
		};
	}

	InputFile.close();										// nothing to do now any more
	return result;
}

//-------------------------------------------------------------------------------------------------
bool SondeTrackReader::EvaluateFirstLine()
{
	if(GroundTrackFileFormat.EvaluateFirstLine(Text))
	{
		return true;
	}
	else
	{
		return false;
	}
}

//-------------------------------------------------------------------------------------------------
bool SondeTrackReader::EvaluateLine(Position& currentPosition)
{
	bool result = false;
	bool evalResult;

	if(		InputFileFormat != GroundTrackFormat::FORMAT_NOT_DETECTED
		&&	InputFileFormat != GroundTrackFormat::FORMAT_UNKNOWN
		)
	{
		if(!FirstLineEvaluated)
		{
			evalResult = EvaluateFirstLine();
			FirstLineEvaluated = evalResult;
		}
		else
		{
			evalResult = GroundTrackFileFormat.EvaluateLine(Text, false);
		}

		if(evalResult)
		{
			if(GetPosition(GroundTrackFileFormat.Content.Pos, currentPosition))
			{
				if(Altitude >= ActualBurstAltitude)						// burst altitude = max. altitude
				{
					ActualBurstAltitude = Altitude;
					PhaseChange = false;

					if(CurrentPhase == Position::PHASE_DESCENT)
					{
						QTextStream(stdout) << "    At altitude " << lround(Altitude) << "m: "
											<< "ascent after descent" << endl;
						QTextStream(stdout).flush();
					}

					CurrentPhase = Position::PHASE_ASCENT;
#ifdef USE_BLOCK_FILTER
#else
					DescentEvent = false;
#endif
				}
				else													// descending
				{
					if(!PhaseChange)									// phase change starts
					{
						PhaseChange = true;
#ifdef USE_BLOCK_FILTER
						PhaseChangeTime = currentPosition.RelativeTime;
#else
						PhaseChangeAltitude = Altitude;
#endif
					}

#ifdef USE_BLOCK_FILTER
					if(currentPosition.RelativeTime - PhaseChangeTime >= Parameter::PHASE_CHANGE_DELAY)
					{
						if(CurrentPhase != Position::PHASE_DESCENT)
						{
							CurrentPhase = Position::PHASE_DESCENT;
							FilteredBlock.Clear();								// restart interpolation
							FilteredVelo.Clear();								// restart interpolation
							FilteredVeloRatio.Clear();							// restart interpolation

							QTextStream(stdout) << "!!! Begin of descent, altitude " << Altitude << "m" << endl;
							QTextStream(stdout).flush();
						}

						result = true;
					}
#else
					else												// phase change runs
					if(PhaseChangeAltitude - Altitude >= Parameter::PHASE_CHANGE_ALT_DELTA)
					{
						if(CurrentPhase != Position::PHASE_DESCENT)
						{
							CurrentPhase = Position::PHASE_DESCENT;
							DescentEvent = true;

							QTextStream(stdout) << "!!! Begin of descent, altitude " << Altitude << "m" << endl;
							QTextStream(stdout).flush();
						}
						PhaseChange = false;
					}
#endif
				}

#ifdef USE_BLOCK_FILTER
#else
				result = StartReached();								// delay after burst
#endif
				BlockCount.Good ++;
			}
			else
			{
//				QTextStream(stdout) << "    Invalid raw ground track values discarded" << endl;
//				QTextStream(stdout).flush();
				BlockCount.Bad ++;
			}
		}
		else
		{
			if(GroundTrackFileFormat.BlockAborted())
			{
				QTextStream(stdout) << "    Invalid raw ground track block discarded" << endl;
				QTextStream(stdout).flush();
				BlockCount.Bad ++;
			} //else block not completed
		}
	}
	else
	{
		QTextStream(stdout) << "    Ground track has invalid sonde type" << endl;
		QTextStream(stdout).flush();
		BlockCount.Bad ++;
	}

	return result;
}

#ifdef USE_BLOCK_FILTER
//-------------------------------------------------------------------------------------------------
bool SondeTrackReader::GetPosition(Position& rawPosition, Position& newPosition)
{
	bool result = false;

	if(StartUpCompareCount < Parameter::START_UP_IGNORE_COUNT)
	{
		StartUpCompareCount ++;
	}
	else
	{
		FilteredBlock.AddBlock			(rawPosition);
		FilteredBlock.GetCurrentPosition(newPosition);

		if(		newPosition.Valid
			&& !newPosition.AltVeloValid)					// not directly from sonde
		{
			double timeDelta	= newPosition.RelativeTime	- LastTime;
			double altDelta		= newPosition.Altitude		- LastAltitude;

			BlockFilterLevel1::Input			rawAltVelo;
			BlockFilterDetail::Level1OutputT	newAltVelo;
			rawAltVelo.AltVelo = timeDelta == 0 ? LastAltVelo : altDelta / timeDelta;

			if(rawAltVelo.AltVelo >= Parameter::PLAUSI_MIN_V_ALT	&& rawAltVelo.AltVelo <= Parameter::PLAUSI_MAX_V_ALT)
			{
				FilteredVelo.Add(rawAltVelo);
				FilteredVelo.GetCurrent(newAltVelo);
				newPosition.AltVelo			= newAltVelo.AltVelo;
				newPosition.AltVeloValid	= newAltVelo.Valid;
			}
			else
			{
				newPosition.Valid = false;
			}
		}

		NewAltitude	= newPosition.Altitude;
		Altitude	= newPosition.Altitude;

		result = newPosition.Valid;
	} //else start up

	LastTime		= newPosition.RelativeTime;
	LastAltitude	= newPosition.Altitude;
	LastAltVelo		= newPosition.AltVelo;

	return result;
}
#else
//-------------------------------------------------------------------------------------------------
bool SondeTrackReader::DeltaInRange
	(
		double			delta,
		double			value,
		double&			compareValue,
		const QString&	text,
		const double	absoluteTolerance
	)
{
	bool result = false;

	double shall = absoluteTolerance;
	result = fabs(delta) <= shall;

	if(result)
	{
		double tolerance = absoluteTolerance / Parameter::COMPARE_PT1_FACTOR;
		double shall1 = compareValue - tolerance;
		double shall2 = compareValue + tolerance;

		result = value >= shall1 && value <= shall2;

//		QTextStream(stdout)	<< " delta=" << delta
//							<< " value=" << value
//							<< " compareValue=" << compareValue
//							<< " compareDiff=" << compareDiff
//							<< " tolerance=" << tolerance
//							<< " shall1=" << shall1
//							<< " shall2=" << shall2
//							<< " ToleranceFactor=" << ToleranceFactor
//							<< endl;

		if(!result)
		{
			QTextStream(stdout)	<< "    At altitude " << lround(NewAltitude) << "m: "
								<< "inexact ground track " << text << " discarded"
								<< " (" << value << " shall ";
			if(shall1 <= shall2)
			{
				QTextStream(stdout)	<< shall1 << "..." << shall2;
			}
			else
			{
				QTextStream(stdout)	<< shall2 << "..." << shall1;
			}
			QTextStream(stdout)	<< ")" << endl;
			QTextStream(stdout).flush();
		}

		compareValue = compareValue	+ (value - compareValue) * Parameter::COMPARE_PT1_FACTOR;
	}
	else
	{
		QTextStream(stdout)	<< "    At altitude " << lround(NewAltitude) << "m: "
							<< "inexact ground track " << text << " delta discarded"
							<< " (" << fabs(delta) << " shall <= " << shall << ")" << endl;
		QTextStream(stdout).flush();
	}

	return result;
}

//-------------------------------------------------------------------------------------------------
bool SondeTrackReader::GetPosition(Position& rawPosition, Position& newPosition)
{
	bool result = false;

	NewAltitude = rawPosition.Altitude;

	double timeDelta	= rawPosition.RelativeTime	- LastTime;
	double altDelta		= rawPosition.Altitude		- LastAltitude;
	double latDelta		= rawPosition.Latitude		- LastLatitude;
	double lonDelta		= rawPosition.Longitude		- LastLongitude;
	double altVelo;											// altitude velocity [m/s]

	if(rawPosition.AltVeloValid)							// directly from sonde
	{
		altVelo = rawPosition.AltVelo;
	}
	else
	{
		altVelo = timeDelta != 0 ? altDelta / timeDelta : LastAltVelo;
	}

	double altVeloDelta	= altVelo - LastAltVelo;

	if(StartUpCompareCount < Parameter::START_UP_IGNORE_COUNT)
	{
		StartUpCompareCount ++;
	}
	else
	if(StartUpCompareCount < Parameter::START_UP_AVERAGE_COUNT)
	{
		CompareTime		+= rawPosition.RelativeTime;
		CompareAlt		+= rawPosition.Altitude;
		CompareLat		+= rawPosition.Latitude;
		CompareLon		+= rawPosition.Longitude;
		CompareAltVelo	+= altVelo;

		StartUpCompareCount ++;
	}

	if(StartUpCompareCount == Parameter::START_UP_AVERAGE_COUNT)
	{
		double count = static_cast<double>(Parameter::START_UP_AVERAGE_COUNT - Parameter::START_UP_IGNORE_COUNT);
		CompareTime		= CompareTime		/ count;
		CompareAlt		= CompareAlt		/ count;
		CompareLat		= CompareLat		/ count;
		CompareLon		= CompareLon		/ count;
		CompareAltVelo	= CompareAltVelo	/ count;

		StartUpCompareCount ++;								// last incrementation
	}

	if(StartUpCompareCount > Parameter::START_UP_AVERAGE_COUNT)
	{
		if(DeltaInRange(timeDelta,		rawPosition.RelativeTime,	CompareTime,"time",		Parameter::TIME_TOLERANCE))
		if(DeltaInRange(altDelta,		rawPosition.Altitude,		CompareAlt,	"altitude",	Parameter::ALTITUDE_TOLERANCE))
		if(DeltaInRange(latDelta,		rawPosition.Latitude,		CompareLat,	"latitude",	Param.LatTolerance))
		if(DeltaInRange(lonDelta,		rawPosition.Longitude,		CompareLon,	"longitude",Param.LonTolerance))
		if(DeltaInRange(altVeloDelta,	altVelo,				CompareAltVelo,	"altVelo",	Parameter::ALT_VELO_TOLERANCE))
		{
			double filteredAltVelo;
			if(CalculateAltitudeVelocity(rawPosition, altVelo, filteredAltVelo))
			{
				newPosition = rawPosition;
				newPosition.AltVelo			= filteredAltVelo;
				newPosition.AltVeloValid	= true;

				Altitude = rawPosition.Altitude;
				result = true;

			}
		} //else message by DeltaInRange
	} //else start up

	LastTime			= rawPosition.RelativeTime;
	LastAltitude		= rawPosition.Altitude;
	LastLatitude		= rawPosition.Latitude;
	LastLongitude		= rawPosition.Longitude;
	LastAltVelo			= altVelo;

	return result;
}

//-------------------------------------------------------------------------------------------------
bool SondeTrackReader::CalculateAltitudeVelocity(Position& currentPosition, double rawAltVelo, double& filteredAltVelo)
{
	bool result = true;

	result = FilteredAltVelo.GetNewFilteredValue(lround(currentPosition.RelativeTime), rawAltVelo);
	filteredAltVelo = FilteredAltVelo.FilteredValue();

	if(!result)
	{
		QTextStream(stdout) << "    At altitude " << lround(currentPosition.Altitude) << "m: ";

		if(CurrentPhase == Position::PHASE_DESCENT)
		{
			QTextStream(stdout) << "waiting for ground track position completion"
								<< " (nominal count = " << FilteredAltVelo.ValueCount()
								<< "/"					<< FilteredAltVelo.NominalCount()
								<< ", minimum count = " << FilteredAltVelo.SumCount()
								<< "/"					<< FilteredAltVelo.MinimumCount()
								<< ")" << endl;
		}
		else
		{
			QTextStream(stdout) << "waiting for descent (ground track position completion)" << endl;
		}
		QTextStream(stdout).flush();
	}

	return result;
}

//-------------------------------------------------------------------------------------------------
bool SondeTrackReader::StartReached()
{
	bool result = false;

	if(CurrentPhase == Position::PHASE_DESCENT)
	{
		if(DescentEvent)
		{
			StartTime = LineTime + Parameter::MIN_TIME;
			DescentEvent = false;
		}

		if(LineTime >= StartTime)
		{
			result = true;
		}
		else
		{
//			double remainingTime = StartTime - LineTime;
//			QTextStream(stdout)	<< "    At altitude " << lround(Altitude) << "m: "
//								<< "waiting for start, " << remainingTime << "s remaining" << endl;
//			QTextStream(stdout).flush();
		}
	}
	else
	{
//		QTextStream(stdout)	<< "    At altitude " << lround(Altitude) << "m: "
//							<< "waiting for descent" << endl;
//		QTextStream(stdout).flush();
	}

	return result;
}
#endif
