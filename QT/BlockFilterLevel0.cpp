#include <math.h>						// fabs

#include "Common.h"
#include "BlockFilterLevel0.h"
#include "Parameter.h"


//-------------------------------------------------------------------------------------------------
BlockFilterLevel0::BlockFilterLevel0(CsvWriter& csv, unsigned long csvMode)
	: Detail(csv, csvMode, BlockList, TimeAverage, Equations, MaxTimeDeviation, MaxDeviations, Interpolation)
{
}

//-------------------------------------------------------------------------------------------------
BlockFilterLevel0::~BlockFilterLevel0()
{
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::Clear()
{
	BlockList.clear();
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::AddBlock(Position& position)
{
	BlockFilterDetail::BlockLevel0T	block(position);
	BlockList.append(block);

	Interpolation.Valid = ApplyNewBlock();

	if(BlockList.size() < static_cast<long>(Parameter::MIN_COUNT))
	{
		Interpolation.Valid = false;
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::GetCurrentPosition(Position& position)
{
	position = Interpolation;
}


//-------------------------------------------------------------------------------------------------
bool BlockFilterLevel0::ApplyNewBlock()
{
	bool result = false;
	unsigned long count = BlockList.size();
	if(count >= 1)
	{
		unsigned long startIndex;

		if(count >= Parameter::FILTER_COUNT)
		{
			startIndex = count - Parameter::FILTER_COUNT;
			count = Parameter::FILTER_COUNT;
		}
		else
		{
			startIndex = 0;
		}

		if(startIndex + count <= static_cast<unsigned long>(BlockList.size()))	// avoid range overflow
		{
			Detail.InitStatistic();
			InitBlockList				(startIndex, count);

			DisableDeviatingTimeBlocks	(startIndex, count);
			DisableDeviatingBlocks		(startIndex, count);

			GetEquation					(startIndex, count);
			GetInterpolation();

			Detail.WriteDetail			(startIndex, count,
										 Parameter::START_UP_IGNORE_COUNT + BlockList.size(),
										 DisableCount, MaxLoopCount
										 );
			result = true;
		}
		else
		{
			QTextStream(stdout) << "BlockFilterLevel0::ApplyNewBlock: startIndex=" << startIndex << " count=" << count << " size=" << BlockList.size() << endl;
			QTextStream(stdout).flush();
		}
	}

	return result;
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::InitBlockList(unsigned long startIndex, unsigned long count)
{
	for(unsigned long i = startIndex; i < startIndex + count; i++)
	{
		BlockList[i].Init();
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::DisableDeviatingTimeBlocks(unsigned long startIndex, unsigned long count)
{
	long filterCount = Parameter::FILTER_COUNT;				// normal count
	if(filterCount > BlockList.size())						// less available
	{
		filterCount = BlockList.size();						// limit to available count
	}

	DisableCount = static_cast<unsigned long>(static_cast<float>(filterCount) * Parameter::TIME_DISABLE_RATE);
	long maxLoopCount = DisableCount;

	Detail.InitTimeStatistic();

	while(		maxLoopCount > 0
			&&	DisableCount > 0
		)
	{
		GetTimeAverage			(startIndex, count);
		GetMaxTimeDeviation		(startIndex, count);
		DisableMaxTimeDeviation	(startIndex, count);		// decrements DisableCount by 1...5 (member count of MaxDeviationsT)

		maxLoopCount --;

		Detail.CollectTimeStatistic1(startIndex, count);
	}

	Detail.CollectTimeStatistic2(startIndex, count);
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::DisableDeviatingBlocks(unsigned long startIndex, unsigned long count)
{
	GetCount(BlockList.size());

	while(		MaxLoopCount > 0
			&&	DisableCount > 0
		)
	{
		GetEquation			(startIndex, count);
		GetDeviationFromEq	(startIndex, count);
		GetMaxDeviations	(startIndex, count);
		DisableMaxDeviations();								// decrements DisableCount by 1...5 (member count of MaxDeviationsT)

		MaxLoopCount --;
		Detail.CollectStatistic1(startIndex, count);
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::GetTimeAverage(unsigned long startIndex, unsigned long count)
{
	TimeAverage.Value = 0.;
	unsigned long averageCount = 0;

	for(unsigned long i = startIndex; i < startIndex + count; i++)
	{
		if(BlockList[i].Enabled)
		{
			TimeAverage.Value += BlockList[i].Block.RelativeTime;
			averageCount ++;
		}
	}

	if(averageCount > 0)									// avoid division by zero
	{
		TimeAverage.Value /= averageCount;
		TimeAverage.Enabled = true;
	}
	else
	{
		TimeAverage.Enabled = false;
		QTextStream(stdout) << "BlockFilterLevel0::GetTimeAverage: startIndex=" << startIndex << " count=" << count << " size=" << BlockList.size() << endl;
		QTextStream(stdout).flush();
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::GetMaxTimeDeviation(unsigned long startIndex, unsigned long count)
{
	MaxTimeDeviation.Init();

	if(TimeAverage.Enabled)
	{
		for(unsigned long i = startIndex; i < startIndex + count; i++)
		{
			if(BlockList[i].Enabled)
			{
				BlockList[i].Deviation.RelativeTime	= fabs(BlockList[i].Block.RelativeTime - TimeAverage.Value);

				if(BlockList[i].Deviation.RelativeTime >= MaxTimeDeviation.Value)
				{
					MaxTimeDeviation.Value		= BlockList[i].Deviation.RelativeTime;
					MaxTimeDeviation.Index		= i;
					MaxTimeDeviation.Enabled	= true;
				}
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::DisableMaxTimeDeviation(unsigned long startIndex, unsigned long count)
{
	if(MaxTimeDeviation.Enabled)
	{
		if(MaxTimeDeviation.Index < BlockList.size())
		{
			if(DisableCount > 0)							// don't disable more than required
			{
				BlockList[MaxTimeDeviation.Index].Enabled = false;
				DisableCount --;
				BlockList[MaxTimeDeviation.Index].DisableCount ++;
			}
			else
			{
				QTextStream(stdout) << "BlockFilterLevel0::DisableMaxDeviationsSub: DisableCount=" << DisableCount << endl;
				QTextStream(stdout).flush();
			}
		}
		else
		{
			QTextStream(stdout) << "BlockFilterLevel0::DisableMaxDeviationsSub: index=" << MaxTimeDeviation.Index << " size=" << BlockList.size() << endl;
			QTextStream(stdout).flush();
		}
	}
	else
	{
		long lastIndex = startIndex + count - 1;
		if(		lastIndex >= 0
			&&	lastIndex == BlockList.size() - 1
			)
		{
			BlockList[lastIndex].Enabled = false;
		}
		else
		{
			QTextStream(stdout) << "BlockFilterLevel0::DisableMaxTimeDeviation: lastIndex=" << lastIndex
								<< " BlockList.size()=" << BlockList.size() << endl;
			QTextStream(stdout).flush();
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::GetEquation(unsigned long startIndex, unsigned long count)
{
	Equations.Valid = false;

	Equations.Altitude.Init();
	Equations.AltVelo.Init();
	Equations.Latitude.Init();
	Equations.Longitude.Init();

	unsigned long end = startIndex + count;
	unsigned long i;
	unsigned long averageCount = 0;

	for(i = startIndex; i < end; i++)
	{
		if(BlockList[i].Enabled)
		{
			double time = BlockList[i].Block.RelativeTime;

			GetEquation1(Equations.Altitude,  time, BlockList[i].Block.Altitude);
			GetEquation1(Equations.AltVelo,   time, BlockList[i].Block.AltVelo);
			GetEquation1(Equations.Latitude,  time, BlockList[i].Block.Latitude);
			GetEquation1(Equations.Longitude, time, BlockList[i].Block.Longitude);

			Equations.LastTime = time;
			averageCount ++;
		}
	}

	if(averageCount > 0)								// avoid division by zero
	{
		Equations.Valid =
				GetEquation2(Equations.Altitude,	averageCount)
			&&	GetEquation2(Equations.AltVelo,		averageCount)
			&&	GetEquation2(Equations.Latitude,	averageCount)
			&&	GetEquation2(Equations.Longitude,	averageCount);
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::GetDeviationFromEq(unsigned long startIndex, unsigned long count)
{
	if(Equations.Valid)
	{
		for(unsigned long i = startIndex; i < startIndex + count; i++)
		{
			if(BlockList[i].Enabled)
			{
				BlockList[i].Deviation.Altitude	= DeviationFromEq(BlockList[i].Block.RelativeTime, Equations.Altitude,	BlockList[i].Block.Altitude);
				BlockList[i].Deviation.Latitude	= DeviationFromEq(BlockList[i].Block.RelativeTime, Equations.Latitude,	BlockList[i].Block.Latitude);
				BlockList[i].Deviation.Longitude= DeviationFromEq(BlockList[i].Block.RelativeTime, Equations.Longitude,	BlockList[i].Block.Longitude);
				BlockList[i].Deviation.AltVelo	= DeviationFromEq(BlockList[i].Block.RelativeTime, Equations.AltVelo,		BlockList[i].Block.AltVelo);

				BlockList[i].Deviation.Valid = true;
			}
			else
			{
				BlockList[i].Deviation.Valid = false;
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::GetMaxDeviations(unsigned long startIndex, unsigned long count)
{
	MaxDeviations.Init();

	for(unsigned long i = startIndex; i < startIndex + count; i++)
	{
		if(		BlockList[i].Enabled
			&&	BlockList[i].Deviation.Valid
			)
		{
			GetMaxDeviationsSub(i, BlockList[i].Deviation.RelativeTime,	MaxDeviations.RelativeTime);
			GetMaxDeviationsSub(i, BlockList[i].Deviation.Altitude,		MaxDeviations.Altitude);
			GetMaxDeviationsSub(i, BlockList[i].Deviation.Latitude,		MaxDeviations.Latitude);
			GetMaxDeviationsSub(i, BlockList[i].Deviation.Longitude,	MaxDeviations.Longitude);
			GetMaxDeviationsSub(i, BlockList[i].Deviation.AltVelo,		MaxDeviations.AltVelo);
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::DisableMaxDeviations()
{
	DisableMaxDeviationsSub(MaxDeviations.RelativeTime);
	DisableMaxDeviationsSub(MaxDeviations.Altitude);
	DisableMaxDeviationsSub(MaxDeviations.AltVelo);
	DisableMaxDeviationsSub(MaxDeviations.Latitude);
	DisableMaxDeviationsSub(MaxDeviations.Longitude);
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::DisableMaxDeviationsSub(BlockFilterDetail::MaxDeviationT& maxDeviation)
{
	if(maxDeviation.Enabled)
	{
		unsigned long i = maxDeviation.Index;

		if(i < static_cast<unsigned long>(BlockList.size()))
		{
			if(		BlockList[i].Enabled
				&&	DisableCount > 0						// don't disable more than required
				)
			{
				BlockList[i].Enabled = false;
				DisableCount --;
			}
		}
		else
		{
			QTextStream(stdout) << "BlockFilterLevel0::DisableMaxDeviationsSub: index=" << i << " size=" << BlockList.size() << endl;
			QTextStream(stdout).flush();
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel0::GetInterpolation()
{
	if(Equations.Valid)
	{
		Interpolation.Altitude		= InterpolationY(Equations.Altitude,	Equations.LastTime);
		Interpolation.AltVelo		= InterpolationY(Equations.AltVelo,		Equations.LastTime);
		Interpolation.Latitude		= InterpolationY(Equations.Latitude,	Equations.LastTime);
		Interpolation.Longitude		= InterpolationY(Equations.Longitude,	Equations.LastTime);
		Interpolation.RelativeTime	= Equations.LastTime;

		Interpolation.Valid = true;
	}
	else
	{
		Interpolation.Valid = false;
	}
}

//-------------------------------------------------------------------------------------------------
//void BlockFilterLevel0::GetAverage(unsigned long startIndex, unsigned long count)
//{
//	Average.Valid = false;									// initialize

//		Average.RelativeTime	= 0;
//		Average.Altitude		= 0;
//		Average.Latitude		= 0;
//		Average.Longitude		= 0;
//		Average.AltVelo			= 0;
//		Average.AltVeloValid	= false;

//		unsigned long averageCount = 0;

//		for(unsigned long i = startIndex; i < startIndex + count; i++)
//		{
//			if(BlockList[i].Enabled)
//			{
//				Average.RelativeTime	+= BlockList[i].Block.RelativeTime;
//				Average.Altitude		+= BlockList[i].Block.Altitude;
//				Average.Latitude		+= BlockList[i].Block.Latitude;
//				Average.Longitude		+= BlockList[i].Block.Longitude;
//				Average.AltVelo			+= BlockList[i].Block.AltVelo;
//				Average.AltVeloValid	 = BlockList[i].Block.AltVeloValid;

//				averageCount ++;
//			}
//		}

//		if(averageCount > 0)								// avoid division by zero
//		{
//			Average.RelativeTime	/= averageCount;
//			Average.Altitude		/= averageCount;
//			Average.Latitude		/= averageCount;
//			Average.Longitude		/= averageCount;
//			Average.AltVelo			/= averageCount;

//			Average.Valid = true;
//		}
//}

//-------------------------------------------------------------------------------------------------
//void BlockFilterLevel0::GetDeviationFromAvg(unsigned long startIndex, unsigned long count)
//{
//		if(Average.Valid)
//		{
//			for(unsigned long i = startIndex; i < startIndex + count; i++)
//			{
//				if(BlockList[i].Enabled)
//				{
//					BlockList[i].Deviation.RelativeTime	= fabs(BlockList[i].Block.RelativeTime	- Average.RelativeTime);
//					BlockList[i].Deviation.Altitude		= fabs(BlockList[i].Block.Altitude		- Average.Altitude);
//					BlockList[i].Deviation.Latitude		= fabs(BlockList[i].Block.Latitude		- Average.Latitude);
//					BlockList[i].Deviation.Longitude	= fabs(BlockList[i].Block.Longitude		- Average.Longitude);
//					BlockList[i].Deviation.AltVelo		= fabs(BlockList[i].Block.AltVelo		- Average.AltVelo);

//					BlockList[i].Deviation.Valid = true;
//				}
//			}
//		}
//}
