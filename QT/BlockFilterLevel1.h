#ifndef BlockFilterLevel1_H
#define BlockFilterLevel1_H

#include "DetailWriterLevel1.h"
#include "BlockFilter.h"
#include "BlockFilterDetail.h"


class CsvWriter;
class BlockFilterLevel0;


class BlockFilterLevel1 : BlockFilter
{
	// constants ----------------------------------------------------------------------------------
	private:

	// types --------------------------------------------------------------------------------------
	public:
		class Input
		{
			public:
				double	AltVelo;

			public:
				Input()
				{
					AltVelo	= 0;
				}
		};

	// variables ----------------------------------------------------------------------------------
	public:
		BlockFilterDetail::EquationsLevel1T		Equations;
	private:
		BlockFilterDetail::Level1OutputT		Result;
		DetailWriterLevel1						Detail;
		BlockFilterDetail::BlockListLevel1T		BlockList;
		BlockFilterLevel0&						FilteredBlock;
		BlockFilterDetail::MaxDeviationsLevel1T	MaxDeviations;

	// functions ----------------------------------------------------------------------------------
	public:
		BlockFilterLevel1
			(
				CsvWriter&			csv,
				unsigned long		csvMode,
				BlockFilterLevel0&	filteredBlock
			);

		void Clear();
		void Add		(Input&		input);
		void GetCurrent	(BlockFilterDetail::Level1OutputT&	output);

	private:
		void InitBlockList
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void DisableDeviatingBlocks
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void GetEquation
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void GetDeviationFromEq(unsigned long startIndex, unsigned long count);
		void GetMaxDeviations
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void DisableMaxDeviations();
		void DisableMaxDeviationsSub
			(
				BlockFilterDetail::MaxDeviationT&	maxDeviation
			);
		void GetInterpolation();
};

#endif
