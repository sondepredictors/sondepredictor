#include <QDateTime>

#include "Common.h"
#include "GroundTrackFormat.h"
#include "Parameter.h"


const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_PREFIX_RS92	= "groundtrack";
const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_PREFIX_M10	= "digitalsonde";
const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_PREFIX_DFM06	= "groundtrack";
const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_PREFIX_C34	= "digitalsonde";

const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_MARKER_M10	= "_M10";
const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_MARKER_DFM06	= "_DFM06";
const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_MARKER_C34	= "_C34";

const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_SUFFIX_RS92	= "txt";
const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_SUFFIX_M10	= "log";
const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_SUFFIX_DFM06	= "txt";
const QString	GroundTrackFormat::GROUND_TRACK_FILE_NAME_SUFFIX_C34	= "txt";

const QString	GroundTrackFormat::NAME_DFM06		= "DFM06";
const QString	GroundTrackFormat::NAME_RS92		= "RS92";
const QString	GroundTrackFormat::NAME_M10			= "M10";
const QString	GroundTrackFormat::NAME_C34			= "C34";

const QString	GroundTrackFormat::SEPARATOR_COMMA	= ",";
const QString	GroundTrackFormat::SEPARATOR_BLANC	= " ";


//-------------------------------------------------------------------------------------------------
GroundTrackFormat::GroundTrackFormat
	(
		Format		inputFileFormat,
		QObject*	parent
	)
:	QObject(parent)
{
	FileFormat	= inputFileFormat;
}

//-------------------------------------------------------------------------------------------------
GroundTrackFormat::~GroundTrackFormat()
{
}

//-------------------------------------------------------------------------------------------------
GroundTrackFormat::Format GroundTrackFormat::DetectFormat(QFileInfo fileInfo)
{
	if(fileInfo.isFile())
	{
		if(		fileInfo.completeBaseName().startsWith(	GROUND_TRACK_FILE_NAME_PREFIX_M10)
			&&	fileInfo.completeBaseName().contains(	GROUND_TRACK_FILE_NAME_MARKER_M10)
			&&	fileInfo.suffix() ==					GROUND_TRACK_FILE_NAME_SUFFIX_M10
			)
		{
			FileFormat = FORMAT_M10;
		}
		else
		if(		fileInfo.completeBaseName().startsWith(	GROUND_TRACK_FILE_NAME_PREFIX_DFM06)
			&&	fileInfo.completeBaseName().contains(	GROUND_TRACK_FILE_NAME_MARKER_DFM06)
			&&	fileInfo.suffix() ==					GROUND_TRACK_FILE_NAME_SUFFIX_DFM06
			)
		{
			FileFormat = FORMAT_DFM06;
		}
		else
		if(		fileInfo.completeBaseName().startsWith(	GROUND_TRACK_FILE_NAME_PREFIX_RS92)
//			&&	fileInfo.completeBaseName().contains(	GROUND_TRACK_FILE_NAME_MARKER_RS92)		// no marker available
			&&	fileInfo.suffix() ==					GROUND_TRACK_FILE_NAME_SUFFIX_RS92
			)
		{
			FileFormat = FORMAT_RS92;
		}
		else
		if(		fileInfo.completeBaseName().startsWith(	GROUND_TRACK_FILE_NAME_PREFIX_C34)
			&&	fileInfo.completeBaseName().contains(	GROUND_TRACK_FILE_NAME_MARKER_C34)
			&&	fileInfo.suffix() ==					GROUND_TRACK_FILE_NAME_SUFFIX_C34
			)
		{
			FileFormat = FORMAT_C34;
		}
		else
		{
			FileFormat = FORMAT_UNKNOWN;
		}
	}
	else
	{
		FileFormat = FORMAT_UNKNOWN;
	}

	return FileFormat;
}

//-------------------------------------------------------------------------------------------------
QString GroundTrackFormat::GroundTrackPrefix()
{
	switch(FileFormat)
	{
		case FORMAT_DFM06:
			FilePrefix = GROUND_TRACK_FILE_NAME_PREFIX_DFM06;
			break;
		case FORMAT_RS92:
			FilePrefix = GROUND_TRACK_FILE_NAME_PREFIX_RS92;
			break;
		case FORMAT_M10:
			FilePrefix = GROUND_TRACK_FILE_NAME_PREFIX_M10;
			break;
		case FORMAT_C34:
			FilePrefix = GROUND_TRACK_FILE_NAME_PREFIX_C34;
			break;
		default:
			FilePrefix = "";
	}

	return FilePrefix;
}

//-------------------------------------------------------------------------------------------------
QString GroundTrackFormat::FormatName(Format fileFormat)
{
	switch(fileFormat)
	{
		case FORMAT_DFM06:	return NAME_DFM06;
		case FORMAT_RS92:	return NAME_RS92;
		case FORMAT_M10:	return NAME_M10;
		case FORMAT_C34:	return NAME_C34;

		case GroundTrackFormat::FORMAT_NOT_DETECTED:
		case GroundTrackFormat::FORMAT_UNKNOWN:
		default:			return "";
	}

	return FilePrefix;
}

//-------------------------------------------------------------------------------------------------
bool GroundTrackFormat::EvaluateFirstLine(QString& text)
{
	bool result = false;

	Content.StartTime = 0.;

	if(EvaluateLine(text, true))
	{
		switch(FileFormat)
		{
			case FORMAT_DFM06:
				Content.StartTime = Content.Pos.RelativeTime;
				result = true;
				break;

			case FORMAT_RS92:
				Content.StartTime = 0.;
				result = true;
				break;

			case FORMAT_M10:
				Content.StartTime = Content.Pos.RelativeTime;
				result = true;
				break;

			case FORMAT_C34:
				Content.StartTime = Content.Pos.RelativeTime;
				result = true;
				break;

			case FORMAT_NOT_DETECTED:
			case FORMAT_UNKNOWN:
			default:
				Content.StartTime = 0.;
				break;
		}

		Content.Pos.RelativeTime = 0.;
	}

	return result;
}

//-------------------------------------------------------------------------------------------------
bool GroundTrackFormat::EvaluateLine(QString& text, bool firstLine)
{
	bool result = false;

	switch(FileFormat)
	{
		case FORMAT_DFM06:				// date, time
		{
			QStringList groups = text.split(SEPARATOR_COMMA, QString::SkipEmptyParts);

			if(groups.size() == 2)
			{
				QStringList group1	= groups[0].split(SEPARATOR_BLANC, QString::SkipEmptyParts);
				QStringList group2	= groups[1].split(SEPARATOR_BLANC, QString::SkipEmptyParts);

				if(		group1.size() == 2
					&&	group2.size() == 6
					)
				{
					QString date = group1[0];
					QString time = group1[1];
					QStringList dateElements = date.split("-", QString::SkipEmptyParts);
					QStringList timeElements = time.split(":", QString::SkipEmptyParts);
					if(		dateElements.size() == 3
						&&	timeElements.size() == 3
						)
					{
						long time = GetTimeFromDateTime(group1[0], group1[1], QString("yyyy-MM-dd hh:mm:ss.zzz"));
						if(time != INVALID_TIME)
						{
							Content.Pos.RelativeTime= static_cast<double>(time) - Content.StartTime;
							Content.Pos.Latitude	= group2[0].toDouble();
							Content.Pos.Longitude	= group2[1].toDouble();
							Content.Pos.Altitude	= group2[2].toDouble();
							Content.Pos.AltVeloValid = false;
							result = true;
						}
					}
				}
			}
			break;
		}

		case FORMAT_RS92:				// seconds
		{
			QStringList group;
			group = text.split(SEPARATOR_BLANC, QString::SkipEmptyParts);

			if(group.size() == 6)
			{
				Content.Pos.RelativeTime= group[0].toDouble();
				Content.Pos.Latitude	= group[1].toDouble();
				Content.Pos.Longitude	= group[2].toDouble();
				Content.Pos.Altitude	= group[3].toDouble();
				Content.Pos.AltVeloValid = false;
				result = true;
			}
			break;
		}

		case FORMAT_M10:					// day time
			{
				QStringList group;
				group	= text.split(SEPARATOR_BLANC, QString::SkipEmptyParts);

				QString dayString	= group[0];
				QString timeString	= group[1];

				if(firstLine)
				{
					Content.StartDay = dayString;
				}

				long time = GetTimeFromDayTime(dayString, timeString);

				if(		time != INVALID_TIME
					&&	group.size() == 16
					&&	group[ 2] == "lat"
					&&	group[ 4] == "lon"
					&&	group[ 6] == "alt"
					&&	group[ 8] == "("
					&&	group[10] == "hP)"
					&&	group[11] == "wind"
					&&	group[14] == "vert"
					)
				{
					Content.Pos.RelativeTime= static_cast<double>(time) - Content.StartTime;
					Content.Pos.Latitude	= group[3].toDouble();
					Content.Pos.Longitude	= group[5].toDouble();
					Content.Pos.Altitude	= group[7].toDouble();
					QString vert = group[15];
					int unitPos = vert.indexOf("m/s");
					if(unitPos >= 0)
					{
						vert = vert.left(unitPos);
						Content.Pos.AltVelo	= vert.toDouble();
						Content.Pos.AltVeloValid = true;
						Content.Pos.Valid = true;
						result = true;
					}
				}
				break;
			}

		case FORMAT_C34:					// day time, block
			if(!text.contains("bad CRC"))
			{
				QStringList group;
				group = text.split(SEPARATOR_BLANC, QString::SkipEmptyParts);

				if(group.size() >= 6)
				{
					if(group[0] == "00FF")
					{
						bool ok;
						unsigned long key = group[1].toULong(&ok, 16);
						if(key <= Block.LastKey)
						{
							if(Block.BlockState != BLOCK_WAIT_DATE)
							{
								Block.Reset = true;
							}
							Block.BlockState = BLOCK_WAIT_DATE;
						}
						Block.LastKey = key;

						switch(Block.BlockState)
						{
							case BLOCK_WAIT_DATE:
								if(		group[1] == "14"
									&&	group[4] == "date"
									)
								{
									QString tempDate = group[5];
									QStringList dateElements = tempDate.split("-", QString::SkipEmptyParts);
									if(dateElements.size() == 3)
									{
										TempDate = "20" + tempDate;
										Block.BlockState = BLOCK_WAIT_TIME;
									}
								}
								break;
							case BLOCK_WAIT_TIME:
								if(		group[1] == "15"
									&&	group[4] == "time"
									)
								{
									QString tempTime = group[5];
									QStringList timeElements = tempTime.split(":", QString::SkipEmptyParts);
									if(timeElements.size() == 3)
									{
										long time = GetTimeFromDateTime(TempDate, tempTime, QString("yyyy-MM-dd hh:mm:ss"));
										if(time != INVALID_TIME)
										{
											Content.Pos.RelativeTime= static_cast<double>(time) - Content.StartTime;
											Block.BlockState = BLOCK_WAIT_LAT;
										}
									}
								}
								break;
							case BLOCK_WAIT_LAT:
								if(		group[1] == "16"
									&&	group[4] == "lat"
									)
								{
									Content.Pos.Latitude	= group[5].toDouble();
									Block.BlockState = BLOCK_WAIT_LON;
								}
								break;
							case BLOCK_WAIT_LON:
								if(		group[1] == "17"
									&&	group[4] == "lon"
									)
								{
									Content.Pos.Longitude	= group[5].toDouble();
									Block.BlockState = BLOCK_WAIT_ALT;
								}
								break;
							case BLOCK_WAIT_ALT:
								if(		group[1] == "18"
									&&	group[4] == "alt"
									)
								{
									Content.Pos.Altitude	= group[5].toDouble();
									Content.Pos.AltVeloValid = false;
									Content.Pos.Valid = true;
									Block.BlockState = BLOCK_WAIT_DATE;
									result = true;
								}
								break;
							default:
								QTextStream(stdout)	<< "Invalid BlockState: " << Block.BlockState << endl;
								Block.BlockState = BLOCK_WAIT_DATE;
								break;
						}
					}
				}
			}
			break;

		default:
			break;
	}

	return result && DataFieldsOk(firstLine);
}

//-------------------------------------------------------------------------------------------------
bool GroundTrackFormat::BlockAborted()
{
	bool result;

	switch(FileFormat)
	{
		case FORMAT_DFM06:
			result = false;
			break;
		case FORMAT_RS92:
			result = false;
			break;
		case FORMAT_M10:
			result = false;
			break;
		case FORMAT_C34:
			result = Block.Reset;
			Block.Reset = false;
			break;
		default:
			result = false;
			break;
	}

	return result;
}

//-------------------------------------------------------------------------------------------------
long GroundTrackFormat::GetTimeFromDateTime(QString& date, QString& time, QString format)
{
	long timeValue;
	QString text = date + " " + time;
	text.remove("\r");
	text.remove("\n");
	QDateTime dateTime = QDateTime::fromString(text, format);
	if(dateTime.isValid())
	{
		timeValue = dateTime.toTime_t();
	}
	else
	{
		timeValue = INVALID_TIME;
	}

	return timeValue;
}

//-------------------------------------------------------------------------------------------------
long GroundTrackFormat::GetTimeFromDayTime(QString& day, QString& time)
{
	long timeValue;
	QString format = "hh:mm:ss.zzz";
	QDateTime dateTime = QDateTime::fromString(time, format);
	if(dateTime.isValid())
	{
		QDate date;
		date.setDate(2000,1,1);								// must be greater than 1970 (UNIX)
		dateTime.setDate(date);
		if(day == Content.StartDay)
		{
			timeValue = dateTime.toTime_t();				// [s]
		}
		else
		{
			timeValue = dateTime.toTime_t() + 3600*24;		// [s]
		}
	}
	else
	{
		timeValue = INVALID_TIME;
	}

	return timeValue;
}

//-------------------------------------------------------------------------------------------------
bool GroundTrackFormat::DataFieldsOk(bool firstLine)
{
	double maxTime = firstLine ? 2000000000. : Parameter::PLAUSI_MAX_TIME;
	bool result =	Content.Pos.RelativeTime>= Parameter::PLAUSI_MIN_TIME		&&	Content.Pos.RelativeTime<= maxTime
				&&	Content.Pos.Latitude	>= Parameter::PLAUSI_MIN_LATITUDE	&&	Content.Pos.Latitude	<= Parameter::PLAUSI_MAX_LATITUDE
				&&	Content.Pos.Longitude	>= Parameter::PLAUSI_MIN_LONGITUDE	&&	Content.Pos.Longitude	<= Parameter::PLAUSI_MAX_LONGITUDE
				&&	Content.Pos.Altitude	>= Parameter::PLAUSI_MIN_ALTITUDE	&&	Content.Pos.Altitude	<= Parameter::PLAUSI_MAX_ALTITUDE;
	Content.Pos.Valid = result;

	if(Content.Pos.AltVeloValid)
	{
		result =	result
				&&	(Content.Pos.AltVelo >= Parameter::PLAUSI_MIN_V_ALT	&& Content.Pos.AltVelo <= Parameter::PLAUSI_MAX_V_ALT);
	}

	return result;
}
