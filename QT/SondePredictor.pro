#-------------------------------------------------
#
# Project created by QtCreator 2015-06-13T18:25:38
#
#-------------------------------------------------

QT	+= core gui
QT	+= serialport
#QT	+= positioning

DEFINES += USE_BLOCK_FILTER
#DEFINES += USE_LINEAR

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET		= SondePredictor
TEMPLATE	= app
#CONFIG		+= mobility
#MOBILITY	+= systeminfo


SOURCES +=	main.cpp\
			mainwindow.cpp \
	BlockFilter.cpp \
	BlockFilterDetail.cpp \
	BlockFilterLevel0.cpp \
	BlockFilterLevel1.cpp \
        BlockFilterLevel2.cpp \
        Configuration.cpp \
	CsvWriter.cpp \
	DetailWriterLevel0.cpp \
	DetailWriterLevel1.cpp \
        DetailWriterLevel2.cpp \
        Execution.cpp \
	LivePrediction.cpp \
	GpsReader.cpp \
	GpxWriter.cpp \
	GroundTrackFormat.cpp \
	KmlWriter.cpp \
	Nmea.cpp \
	Parameter.cpp \
	Position.cpp \
	PredictionTrackReader.cpp \
	SondeTrackReader.cpp \
	XShiftFilter.cpp \
	Version.cpp

HEADERS  +=	mainwindow.h \
	BlockFilter.h \
	BlockFilterLevel0.h \
	BlockFilterLevel1.h \
        BlockFilterLevel2.h \
        BlockFilterDetail.h \
	Common.h \
	Configuration.h \
	CsvWriter.h \
	DetailWriterLevel0.h \
	DetailWriterLevel1.h \
        DetailWriterLevel2.h \
        Execution.h \
	LivePrediction.h \
	GpsReader.h \
	GpxWriter.h \
	GroundTrackFormat.h \
	KmlWriter.h \
	Nmea.h \
	Parameter.h \
	Position.h \
	PredictionTrackReader.h \
	SondeTrackReader.h \
	XShiftFilter.h \
	Version.h

FORMS	+= mainwindow.ui
