#include <math.h>						// fabs

#include "Common.h"
#include "BlockFilter.h"
#include "Parameter.h"


//-------------------------------------------------------------------------------------------------
BlockFilter::BlockFilter()
{
	DisableCount	= 0;
	MaxLoopCount	= 0;
}

//-------------------------------------------------------------------------------------------------
BlockFilter::~BlockFilter()
{
}

//-------------------------------------------------------------------------------------------------
void BlockFilter::GetCount(long listSize)
{
	long filterCount = Parameter::FILTER_COUNT;				// normal count
	if(filterCount > listSize)								// less available
	{
		filterCount = listSize;								// limit to available count
	}

	DisableCount = static_cast<unsigned long>(static_cast<float>(filterCount) * Parameter::DISABLE_RATE);
	MaxLoopCount = DisableCount;
}

#ifdef USE_LINEAR
//-------------------------------------------------------------------------------------------------
void BlockFilter::GetEquation1
	(
		BlockFilterDetail::EquationT& equation,
		double time,
		double value
	)
{
	equation.xm += time;
	equation.ym += value;
	equation.xv += time * time;
	equation.kv += time * value;
}

//-------------------------------------------------------------------------------------------------
bool BlockFilter::GetEquation2
	(
		BlockFilterDetail::EquationT& equation,
		unsigned long averageCount
	)
{
	bool result = false;

	// y = a + b*x
	// https://de.wikipedia.org/wiki/Lineare_Regression
	// http://www.faes.de/Basis/Basis-Statistik/Basis-Statistik-Korrelation-Re/basis-statistik-korrelation-re.html

//	equation.count = averageCount;
//	equation.b =	(equation.kv - equation.xm * equation.ym  / averageCount)
//				/	(equation.xv - equation.xm * equation.xm  / averageCount);
//	equation.a =	(equation.xv * equation.ym - equation.xm * equation.kv)
//				/	(equation.xv - equation.xm * equation.xm);

	// http://www.torsten-horn.de/techdocs/java-approximationsfunktionen.htm
	equation.xm	= equation.xm / averageCount;
	equation.ym	= equation.ym / averageCount;
	equation.xv	= equation.xv / averageCount - equation.xm * equation.xm;
	equation.kv	= equation.kv / averageCount - equation.xm * equation.ym;
	if(equation.xv != 0.)
	{
		equation.b	= equation.kv / equation.xv;
		equation.a	= equation.ym - equation.b * equation.xm;
		result = true;
	}

	return result;
}

//-------------------------------------------------------------------------------------------------
double BlockFilter::DeviationFromEq
	(
		double time,
		BlockFilterDetail::EquationT& equation,
		double blockValue
	)
{
	double y = InterpolationY(equation, time);
	return fabs(blockValue - y);
}

//-------------------------------------------------------------------------------------------------
double BlockFilter::InterpolationY
	(
		BlockFilterDetail::LinearEquationT& eq,
		double time
	)
{
	return eq.a + eq.b * time;													// y = a + b*x
}

#else

//-------------------------------------------------------------------------------------------------
void BlockFilter::GetEquation1
	(
		BlockFilterDetail::EquationT& equation,
		double time,
		double value
	)
{
	double x	= time;
	double x2	= time * x;
	double x3	= time * x2;
	double x4	= time * x3;

	double y	= value;

	equation.x	+= x;
	equation.x2	+= x2;
	equation.x3	+= x3;
	equation.x4	+= x4;
	equation.y	+= y;
	equation.yx2+= y * x2;
	equation.xy += x * y;
}

//-------------------------------------------------------------------------------------------------
bool BlockFilter::GetEquation2
	(
		BlockFilterDetail::EquationT& eq,
		unsigned long averageCount
	)
{
	bool result = false;

	// y = a*x^2 + b*x + c
	// http://www.stksachs.uni-leipzig.de/tl_files/media/pdf/lehrbuecher/informatik/Regressionsanalyse.pdf

	// turn sum into average
	eq.x	= eq.x		/ averageCount;
	eq.x2	= eq.x2		/ averageCount;
	eq.x3	= eq.x3		/ averageCount;
	eq.x4	= eq.x4		/ averageCount;
	eq.y	= eq.y		/ averageCount;
	eq.yx2	= eq.yx2	/ averageCount;
	eq.xy	= eq.xy		/ averageCount;

	// equation
	double term1 = eq.x3 - eq.x * eq.x2;
	double an = (eq.x4 - eq.x2 * eq.x2) * (eq.x2 - eq.x * eq.x) - term1 * term1;
	double bn = eq.x2 - eq.x * eq.x;
	if(an != 0. && bn != 0.)
	{
		eq.a = ((eq.yx2 - eq.y * eq.x2) * (eq.x2 - eq.x * eq.x) - (eq.xy - eq.y * eq.x) * (eq.x3 - eq.x * eq.x2))
				/ an;
		eq.b = (eq.xy - eq.y * eq.x - eq.a * (eq.x3 - eq.x * eq.x2))
				/ bn;
		eq.c = eq.y - eq.a * eq.x2 - eq.b * eq.x;

		result = true;
	}

	return result;
}

//-------------------------------------------------------------------------------------------------
double BlockFilter::DeviationFromEq
	(
		double time,
		BlockFilterDetail::EquationT& equation,
		double blockValue
	)
{
	double y = InterpolationY(equation, time);
	return fabs(blockValue - y);
}

//-------------------------------------------------------------------------------------------------
double BlockFilter::InterpolationY
	(
		BlockFilterDetail::EquationT& eq,
		double time
	)
{
	double x = time;
	return eq.a * x * x + eq.b * x + eq.c;										// y = a*x^2 + b*x + c
}

#endif

//-------------------------------------------------------------------------------------------------
void BlockFilter::GetMaxDeviationsSub
	(
		unsigned long i,
		double deviation,
		BlockFilterDetail::MaxDeviationT& maxDeviation
	)
{
	if(deviation >= maxDeviation.Value)
	{
		maxDeviation.Value		= deviation;
		maxDeviation.Index		= i;
		maxDeviation.Enabled	= true;
	}
}
