#ifndef BlockFilterDetail_H
#define BlockFilterDetail_H

#include <QList>

#include "Position.h"


class BlockFilterDetail
{
	// constants ----------------------------------------------------------------------------------
	public:
		static const unsigned long	DETAIL_MODE_OFF		= 0;
		static const unsigned long	DETAIL_MODE_BASIC	= 1;
		static const unsigned long	DETAIL_MODE_LEVEL_0	= 2;
		static const unsigned long	DETAIL_MODE_LEVEL_1	= 3;
		static const unsigned long	DETAIL_MODE_LEVEL_2	= 4;

	// types --------------------------------------------------------------------------------------
	public:
		class TimeAverageT
		{
			public:
				bool	Enabled;
				double	Value;								// duration since start [s]

			public:
				TimeAverageT()
				{
					Enabled	= false;
					Value	= 0;
				}
		};

		class BlockLevel0T
		{
			public:
				bool		Enabled;
				long		DisableCount;
				Position	Block;
				Position	Deviation;

			public:
				BlockLevel0T(Position& position)
				{
					Enabled			= false;
					DisableCount	= 0;
					Block			= position;
					Deviation.Valid	= false;
				}

				void Init()
				{
					Enabled			= true;
					Deviation.Valid	= false;
				}
		};

		class BlockLevel1T
		{
			public:
				bool		Enabled;
				long		DisableCount;
				double		AltVelo;
				double		Deviation;
				bool		DeviationValid;

			public:
				BlockLevel1T(double altVelo)
				{
					Enabled			= false;
					DisableCount	= 0;
					AltVelo			= altVelo;
					DeviationValid	= false;
				}

				void Init()
				{
					Enabled			= true;
					DeviationValid	= false;
				}
		};

		class BlockLevel2T
		{
			public:
				bool		Enabled;
				long		DisableCount;
				double		AltVeloRatio;
				double		Deviation;
				bool		DeviationValid;

			public:
				BlockLevel2T(double altVeloRatio)
				{
					Enabled				= false;
					DisableCount		= 0;
					AltVeloRatio		= altVeloRatio;
					Deviation			= 0;
					DeviationValid		= false;
				}

				void Init()
				{
					Enabled			= true;
					DeviationValid	= false;
				}
		};

		typedef QList<BlockLevel0T>	BlockListLevel0T;
		typedef QList<BlockLevel1T>	BlockListLevel1T;
		typedef QList<BlockLevel2T>	BlockListLevel2T;

#ifdef USE_LINEAR
		class LinearEquationT
		{
			public:
				double	a;				// offset
				double	b;				// gradient
				double	xm;
				double	ym;
				double	xv;
				double	kv;
				unsigned long	count;

			public:
				void Init()
				{
					a	= 0;
					b	= 0;
					xm	= 0;
					ym	= 0;
					xv	= 0;
					kv	= 0;
					count = 0;
				}
		};

		typedef LinearEquationT	EquationT;
#else
		class SquareEquationT
		{
			public:
				double	a;				// factor of square part
				double	b;				// gradient
				double	c;				// offset
				double	x;				// avg(x)
				double	x2;				// avg(x^2)
				double	x3;				// avg(x^3)
				double	x4;				// avg(x^4)
				double	y;				// avg(y)
				double	yx2;			// avg(y * x^2)
				double	xy;				// avg(x * y)
				unsigned long	count;

			public:
				void Init()
				{
					a	= 0;
					b	= 0;
					c	= 0;
					x	= 0;
					x2	= 0;
					x3	= 0;
					x4	= 0;
					y	= 0;
					yx2	= 0;
					xy	= 0;
					count = 0;
				}
		};

		typedef SquareEquationT	EquationT;
#endif

		class EquationsLevel0T
		{
			public:
				bool		Valid;
				double		LastTime;
				EquationT	Altitude;
				EquationT	AltVelo;
				EquationT	Latitude;
				EquationT	Longitude;
		};

		class EquationsLevel1T
		{
			public:
				bool		Valid;
				double		LastTime;
				EquationT	AltVelo;
		};

		class EquationsLevel2T
		{
			public:
				bool		Valid;
				double		LastTime;
				EquationT	AltVeloRatio;
		};

		class MaxDeviationT
		{
			public:
				bool	Enabled;
				double	Value;
				long	Index;

			public:
				void Init()
				{
					Enabled	= false;
					Value	= 0;
					Index	= ULONG_MAX;					// to detect internal error
				}
		};

		class MaxDeviationsLevel0T
		{
			public:
				MaxDeviationT	Altitude;
				MaxDeviationT	AltVelo;
				MaxDeviationT	Latitude;
				MaxDeviationT	Longitude;
				MaxDeviationT	RelativeTime;
			public:
				void Init()
				{
					Altitude	.Init();
					AltVelo		.Init();
					Latitude	.Init();
					Longitude	.Init();
					RelativeTime.Init();
				}
		};

		class MaxDeviationsLevel1T
		{
			public:
				MaxDeviationT	AltVelo;
			public:
				void Init()
				{
					AltVelo.Init();
				}
		};

		class MaxDeviationsLevel2T
		{
			public:
				MaxDeviationT	AltVeloRatio;
			public:
				void Init()
				{
					AltVeloRatio.Init();
				}
		};

		class Level1OutputT
		{
			public:
				bool	Valid;
				double	AltVelo;

			public:
				Level1OutputT()
				{
					Valid	= false;
					AltVelo	= 0;
				}
		};

		class Level2OutputT
		{
			public:
				bool	Valid;
				double	AltVeloRatio;

			public:
				Level2OutputT()
				{
					Valid			= false;
					AltVeloRatio	= 1;
				}
		};

	// variables ----------------------------------------------------------------------------------
	private:

	// functions ----------------------------------------------------------------------------------
	public:
};

#endif
