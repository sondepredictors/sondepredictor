#include "Common.h"
#include "DetailWriterLevel0.h"
#include "DetailWriterLevel1.h"
#include "BlockFilterLevel0.h"
#include "BlockFilterDetail.h"


//-------------------------------------------------------------------------------------------------
DetailWriterLevel1::DetailWriterLevel1
	(
		CsvWriter&								csv,
		unsigned long							csvMode,
		BlockFilterDetail::BlockListLevel1T&	blockList,
		BlockFilterLevel0&						filteredBlock,
		BlockFilterDetail::Level1OutputT&		interpolation
	)
	: Csv(csv)
	, CsvMode(csvMode)
	, BlockList(blockList)
	, FilteredBlock(filteredBlock)
	, Interpolation(interpolation)
{
}

//-------------------------------------------------------------------------------------------------
DetailWriterLevel1::~DetailWriterLevel1()
{
}

//-------------------------------------------------------------------------------------------------
void DetailWriterLevel1::WriteDetail(unsigned long startIndex, unsigned long count, long lineNumber)
{
	if(		CsvMode == BlockFilterDetail::DETAIL_MODE_BASIC
		||	CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_1
		)
	{
		if(CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_1)
		{
			Csv.AddElement(lineNumber);									// Line
			Csv.AddElement((long)BlockList.size() - 1);					// BlkI
		}

		long lastIndex = startIndex + count - 1;
		if(		lastIndex >= 0
			&&	lastIndex == BlockList.size() - 1
			)
		{
			if(CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_1)
			{
				if(lastIndex < FilteredBlock.BlockList.size())
				{
					Csv.AddElement(FilteredBlock.BlockList[lastIndex].Block.RelativeTime);	// Time
				}
				else
				{
					Csv.AddElement("-");
					QTextStream(stdout) << "DetailWriterLevel1::WriteDetail: lastIndex=" << lastIndex
										<< " FilteredBlock.BlockList.size()=" << FilteredBlock.BlockList.size() << endl;
					QTextStream(stdout).flush();
				}
			}

			Csv.AddElement(BlockList[lastIndex].AltVelo);						// Velo
		}
		else
		{
			if(CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_1)
			{
				Csv.AddElement("-");
			}
			Csv.AddElement("-");
			QTextStream(stdout) << "DetailWriterLevel1::WriteDetail: lastIndex=" << lastIndex
								<< " BlockList.size()=" << BlockList.size() << endl;
			QTextStream(stdout).flush();
		}

		if(CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_1)
		{
			if(FilteredBlock.Equations.Valid)
			{
				Csv.AddElement(FilteredBlock.Equations.LastTime);		// IpoT
			}
			else
			{
				Csv.AddElement("");
			}
		}

		if(Interpolation.Valid)
		{
			Csv.AddElement(Interpolation.AltVelo);						// IpoVelo
		}
		else
		{
			Csv.AddElement("");
		}
	}
}
