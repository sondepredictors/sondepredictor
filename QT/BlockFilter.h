#ifndef BlockFilter_H
#define BlockFilter_H

#include "BlockFilterDetail.h"


class BlockFilter
{
	// constants ----------------------------------------------------------------------------------
	private:

	// types --------------------------------------------------------------------------------------
	public:

	// variables ----------------------------------------------------------------------------------
	public:
		long	DisableCount;
		long	MaxLoopCount;

	// functions ----------------------------------------------------------------------------------
	public:
		BlockFilter();
		virtual ~BlockFilter();

	protected:
		void GetCount(long listSize);
		void GetEquation1
			(
				BlockFilterDetail::EquationT& equation,
				double				time,
				double				value
			);
		bool GetEquation2
			(
				BlockFilterDetail::EquationT& equation,
				unsigned long		averageCount
			);
		double	DeviationFromEq
			(
				double time,
				BlockFilterDetail::EquationT& equation,
				double blockValue
			);
		double	InterpolationY
			(
				BlockFilterDetail::EquationT& equation,
				double time
			);
		void GetMaxDeviationsSub
			(
				unsigned long	i,
				double			deviation,
				BlockFilterDetail::MaxDeviationT&	maxDeviation
			);
};

#endif
