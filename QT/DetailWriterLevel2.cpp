#include "Common.h"
//#include "DetailWriterLevel1.h"
#include "DetailWriterLevel2.h"
#include "BlockFilterLevel1.h"
#include "BlockFilterDetail.h"


//-------------------------------------------------------------------------------------------------
DetailWriterLevel2::DetailWriterLevel2
	(
		CsvWriter&								csv,
		unsigned long							csvMode,
		BlockFilterDetail::BlockListLevel2T&	blockList,
//		BlockFilterLevel0&						filteredBlock,
		BlockFilterDetail::Level2OutputT&		interpolation
	)
	: Csv(csv)
	, CsvMode(csvMode)
	, BlockList(blockList)
//	, FilteredBlock(filteredBlock)
	, Interpolation(interpolation)
{
}

//-------------------------------------------------------------------------------------------------
DetailWriterLevel2::~DetailWriterLevel2()
{
}

//-------------------------------------------------------------------------------------------------
void DetailWriterLevel2::WriteDetail(unsigned long startIndex, unsigned long count, long lineNumber)
{
	if(		CsvMode == BlockFilterDetail::DETAIL_MODE_BASIC
		||	CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_2
		)
	{
		if(CsvMode == BlockFilterDetail::DETAIL_MODE_LEVEL_2)
		{
			Csv.AddElement(lineNumber);									// Line
			Csv.AddElement((long)BlockList.size() - 1);					// BlkI
		}

		long lastIndex = startIndex + count - 1;
		if(		lastIndex >= 0
			&&	lastIndex == BlockList.size() - 1
			)
		{
			Csv.AddElement(BlockList[lastIndex].AltVeloRatio);			// AltVeloRatio
		}
		else
		{
			Csv.AddElement("-");
			QTextStream(stdout) << "DetailWriterLevel2::WriteDetail: lastIndex=" << lastIndex
								<< " BlockList.size()=" << BlockList.size() << endl;
			QTextStream(stdout).flush();
		}

		if(Interpolation.Valid)
		{
			Csv.AddElement(Interpolation.AltVeloRatio);					// IpoVeloRatio
		}
		else
		{
			Csv.AddElement("");
		}
	}
}
