#ifndef BlockFilterLevel2_H
#define BlockFilterLevel2_H

#include "DetailWriterLevel2.h"
#include "BlockFilter.h"
#include "BlockFilterDetail.h"


class CsvWriter;
class BlockFilterLevel0;
class BlockFilterLevel1;


class BlockFilterLevel2 : BlockFilter
{
	// constants ----------------------------------------------------------------------------------
	private:

	// types --------------------------------------------------------------------------------------
	public:
		class Input
		{
			public:
				double	AltVeloRatio;

			public:
				Input()
				{
					AltVeloRatio		= 0;
				}
		};

	// variables ----------------------------------------------------------------------------------
	public:
		BlockFilterDetail::EquationsLevel2T		Equations;
	private:
		BlockFilterDetail::Level2OutputT		Result;
		DetailWriterLevel2						Detail;
		BlockFilterDetail::BlockListLevel2T		BlockList;
		BlockFilterLevel0&						FilteredBlock;
		BlockFilterDetail::MaxDeviationsLevel2T	MaxDeviations;

	// functions ----------------------------------------------------------------------------------
	public:
		BlockFilterLevel2
			(
				CsvWriter&			csv,
				unsigned long		csvMode,
				BlockFilterLevel0&	filteredBlock
			);

		void Clear();
		void Add(Input &input);
		void GetCurrent	(BlockFilterDetail::Level2OutputT&	output);

	private:
		void InitBlockList
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void DisableDeviatingBlocks
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void GetEquation
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void GetDeviationFromEq(unsigned long startIndex, unsigned long count);
		void GetMaxDeviations
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void DisableMaxDeviations();
		void DisableMaxDeviationsSub
			(
				BlockFilterDetail::MaxDeviationT&	maxDeviation
			);
		void GetInterpolation();
};

#endif
