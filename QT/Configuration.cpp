#include <QFile>
#include <QSettings>

#include "Common.h"
#include "Configuration.h"


const QString		Configuration::CONFIGURATION_FILE_NAME	= "./SondePredictor.ini";
const QString		Configuration::DEFAULT_DIRECTORY_NAME				= ".";
const QString		Configuration::DEFAULT_GPS_NMEA_INPUT_NAME			= "COM4";
const QString		Configuration::DEFAULT_PREDICTION_NMEA_OUTPUT_NAME	= "COM5";
const uint			Configuration::DEFAULT_DETAIL			= 0;
const QString		Configuration::GROUP_FILES				= "DataInterface";
const QString		Configuration::KEY_DIRECTORY			= "Directory";
const QString		Configuration::KEY_GPS_INPUT			= "GpsInput";
const QString		Configuration::KEY_PREDICTION_OUTPUT	= "PredictionOutput";
const QString		Configuration::KEY_DETAIL				= "Detail";

//-------------------------------------------------------------------------------------------------
Configuration::Configuration(QObject* parent)
	: QObject	(parent)
{
}

//-------------------------------------------------------------------------------------------------
Configuration::~Configuration()
{
}

//-------------------------------------------------------------------------------------------------
void Configuration::HandleDefaultFile()
{
	if(!QFile::exists(CONFIGURATION_FILE_NAME))
	{
		Parameter.DirectoryName				= DEFAULT_DIRECTORY_NAME;
		Parameter.GpsNmeaInputName			= DEFAULT_GPS_NMEA_INPUT_NAME;
		Parameter.PredictionNmeaOutputName	= DEFAULT_PREDICTION_NMEA_OUTPUT_NAME;
		Parameter.Detail					= DEFAULT_DETAIL;

		QSettings	settings(CONFIGURATION_FILE_NAME, QSettings::IniFormat);
		settings.beginGroup(GROUP_FILES);
		settings.setValue(KEY_DIRECTORY,			Parameter.DirectoryName);
		settings.setValue(KEY_GPS_INPUT,			Parameter.GpsNmeaInputName);
		settings.setValue(KEY_PREDICTION_OUTPUT,	Parameter.PredictionNmeaOutputName);
		settings.setValue(KEY_DETAIL,				Parameter.Detail);
		settings.endGroup();
		settings.sync();
	}
}

//-------------------------------------------------------------------------------------------------
void Configuration::ReadParameters()
{
	QSettings	settings(CONFIGURATION_FILE_NAME, QSettings::IniFormat);
	settings.beginGroup(GROUP_FILES);
	Parameter.DirectoryName				= settings.value(KEY_DIRECTORY,			DEFAULT_DIRECTORY_NAME).toString();
	Parameter.GpsNmeaInputName			= settings.value(KEY_GPS_INPUT,			DEFAULT_GPS_NMEA_INPUT_NAME).toString();
	Parameter.PredictionNmeaOutputName	= settings.value(KEY_PREDICTION_OUTPUT,	DEFAULT_PREDICTION_NMEA_OUTPUT_NAME).toString();
	Parameter.Detail					= settings.value(KEY_DETAIL,			DEFAULT_DETAIL).toUInt();
	settings.endGroup();
}
