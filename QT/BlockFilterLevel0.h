#ifndef BlockFilterLevel0_H
#define BlockFilterLevel0_H

#include "Position.h"
#include "DetailWriterLevel0.h"
#include "BlockFilterDetail.h"
#include "BlockFilter.h"


class BlockFilterLevel0 : BlockFilter
{
	// constants ----------------------------------------------------------------------------------
	private:

	// types --------------------------------------------------------------------------------------
	private:

	// variables ----------------------------------------------------------------------------------
	public:
		BlockFilterDetail::BlockListLevel0T		BlockList;
	private:
		BlockFilterDetail::TimeAverageT			TimeAverage;
		Position								Interpolation;
		BlockFilterDetail::MaxDeviationT		MaxTimeDeviation;
		BlockFilterDetail::MaxDeviationsLevel0T	MaxDeviations;
	public:
		BlockFilterDetail::EquationsLevel0T		Equations;
	private:
		DetailWriterLevel0						Detail;

	// functions ----------------------------------------------------------------------------------
	public:
		BlockFilterLevel0
			(
				CsvWriter&		csv,
				unsigned long	csvMode
			);
		virtual ~BlockFilterLevel0();

		void Clear();
		void AddBlock(Position& position);
		void GetCurrentPosition(Position& position);

	private:
		bool ApplyNewBlock();
		void InitBlockList
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void DisableDeviatingTimeBlocks
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void DisableDeviatingBlocks
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void GetTimeAverage
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void GetDeviationFromLinearEquation
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void GetMaxTimeDeviation
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void GetMaxDeviations
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void DisableMaxTimeDeviation
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void DisableMaxDeviations();
		void DisableMaxDeviationsSub
			(
				BlockFilterDetail::MaxDeviationT&	maxDeviation
			);
		void GetEquation
			(
				unsigned long	startIndex,
				unsigned long	count
			);
		void	GetInterpolation();
		void	GetDeviationFromEq(unsigned long startIndex, unsigned long count);
};

#endif
