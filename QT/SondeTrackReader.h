#ifndef SondeTrackReader_H
#define SondeTrackReader_H

#include <QTextStream>
#include <QFile>
#include <QFileInfo>
#include <QDateTime>
#include <QVector>

#include "Position.h"
#ifdef USE_BLOCK_FILTER
#include "BlockFilterLevel0.h"
#include "BlockFilterLevel1.h"
#include "BlockFilterLevel2.h"
#else
#include "XShiftFilter.h"
#endif
#include "Parameter.h"
#include "GroundTrackFormat.h"


class SondeTrackReader
{
	// constants ----------------------------------------------------------------------------------
	private:
		static const double	SECONDS_PER_LINE;

	// types --------------------------------------------------------------------------------------
	private:
		class AltitudeVelocity
		{
			public:
				unsigned long	StartIndex;
				unsigned long	EndIndex;
				unsigned long	Count;
				double			Sum;
			public:
				AltitudeVelocity()
				{
					StartIndex	= 0;
					EndIndex	= 0;
					Count		= 0;
					Sum			= 0;
				}
		};

		class BlockCountT
		{
			public:
				unsigned long	Good;
				unsigned long	Bad;

			public:
				BlockCountT()
				{
					Good	= 0;
					Bad		= 0;
				}
		};

	// variables ----------------------------------------------------------------------------------
	public:
		Position::Phase	CurrentPhase;
		double			ActualBurstAltitude;				// actual burst height [m], valid if PHASE_DESCENT
		long			WaitingTime;						// duration [s] waiting for next position
#ifdef USE_BLOCK_FILTER
#else
		bool			DescentEvent;						// descent starts
#endif
		bool			EndOfData;							// input file was read completely
		BlockCountT		BlockCount;							// number of lines/blocks

	private:
		QString			Text;
		QFile&			InputFile;							// input file from SondeMonitor
		QFileInfo		InputFileInfo;						// file system informations of InputFile
		GroundTrackFormat::Format	InputFileFormat;		// file format (sonde type)
		GroundTrackFormat			GroundTrackFileFormat;
		bool			PhaseChange;						// phase may change from ASCENT to DESCENT
#ifdef USE_BLOCK_FILTER
		double			PhaseChangeTime;					// relative time when descent starts
#else
		double			PhaseChangeAltitude;				// altitude when descent seems to start
#endif
		double			NewAltitude;						// new altitude   [m], for message
		double			Altitude;							// last valid altitude [m]

		double			LastTime;							// last time      [s]
		double			LastAltitude;						// last/current altitude [m]
		double			LastLatitude;						// last latitude  [°]
		double			LastLongitude;						// last longitude [°]
		double			LastAltVelo;						// last altitude velocity [m/s]
		double			CompareTime;						// last time      [s]
		double			CompareAlt;							// last altitude  [m]
		double			CompareLat;							// last latitude  [°]
		double			CompareLon;							// last longitude [°]
		double			CompareAltVelo;						// last alt. velo [s]
		unsigned long	StartUpCompareCount;				// number of compare values for start up average

		qint64			InputFilePosition;					// file position when closing intermediate
		double			StartTime;							// time [s] (relative to file start) when to start prediction
		QTextStream&	ResultStream;
		unsigned long	LineTime;							// time [s] since file start
#ifdef USE_BLOCK_FILTER
		BlockFilterLevel0	FilteredBlock;
		BlockFilterLevel1	FilteredVelo;
	public:
		BlockFilterLevel2	FilteredVeloRatio;
	private:
#else
		XShiftFilter	FilteredAltVelo;
#endif
		Parameter		Param;
		bool			FirstLineEvaluated;

	// functions ----------------------------------------------------------------------------------
	public:
		SondeTrackReader
			(
				GroundTrackFormat::Format	inputFileFormat,
				QFile&				inputFile,				// input file
#ifdef USE_BLOCK_FILTER
				CsvWriter&			detail,					// output file for processing details
				unsigned long		csvMode,
#endif
				QTextStream&		resultStream
			);
		bool	NewCurrentPosition							// get new current position
			(												// return: new current position available
				Position&	position						// out:	new current position
			);

	private:
		bool GetPosition									// test if position can be used
			(												// return:	position OK
				Position&	rawPosition,
				Position&	newPosition
			);
#ifdef USE_BLOCK_FILTER
#else
		bool CalculateAltitudeVelocity						// calculate filtered altitude velocity
			(												// result: filteredAltVelo is OK
				Position&	currentPosition,				// current position
				double		rawAltVelo,						// in:	raw altitude velocity
				double&		filteredAltVelo					// out:	filtered altitude velocity
			);
		bool StartReached();
		bool DeltaInRange
			(
				double			delta,
				double			value,
				double&			compareValue,
				const QString&	text,
				const double	absoluteTolerance
			);
#endif
		bool EvaluateLine									// evaluate one line of new current position
			(
				Position&	currentPosition
			);
		bool EvaluateFirstLine();
};

#endif
