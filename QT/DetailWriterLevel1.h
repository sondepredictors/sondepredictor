#ifndef DetailWriterLevel1_H
#define DetailWriterLevel1_H

#include "CsvWriter.h"
#include "BlockFilterDetail.h"

class BlockFilterLevel0;


class DetailWriterLevel1
{
	// constants ----------------------------------------------------------------------------------
	private:

	// types --------------------------------------------------------------------------------------
	private:

	// variables ----------------------------------------------------------------------------------
	private:
		CsvWriter&								Csv;
		unsigned long							CsvMode;
		BlockFilterDetail::BlockListLevel1T&	BlockList;
		BlockFilterLevel0&						FilteredBlock;
		BlockFilterDetail::Level1OutputT&		Interpolation;

	// functions ----------------------------------------------------------------------------------
	public:
		DetailWriterLevel1
			(
				CsvWriter&								csv,
				unsigned long							csvMode,
				BlockFilterDetail::BlockListLevel1T&	blockList,
				BlockFilterLevel0&						filteredBlock,
				BlockFilterDetail::Level1OutputT&		interpolation
			);
		virtual ~DetailWriterLevel1();

		void	WriteDetail
			(
				unsigned long	startIndex,
				unsigned long	count,
				long			lineNumber
			);

	private:
};

#endif
