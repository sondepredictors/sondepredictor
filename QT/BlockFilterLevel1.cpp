#include "Common.h"
#include "BlockFilterDetail.h"
#include "BlockFilterLevel0.h"
#include "BlockFilterLevel1.h"
#include "Parameter.h"


//-------------------------------------------------------------------------------------------------
BlockFilterLevel1::BlockFilterLevel1(CsvWriter& csv, unsigned long csvMode, BlockFilterLevel0& filteredBlock)
	: Detail(csv, csvMode, BlockList, filteredBlock, Result)
	, FilteredBlock(filteredBlock)
{
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::Clear()
{
	BlockList.clear();
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::Add(Input& input)
{
	Result.Valid	= false;								// initialization

	BlockFilterDetail::BlockLevel1T	block(input.AltVelo);
	BlockList.append(block);

	unsigned long count = BlockList.size();
	if(count >= 1)
	{
		unsigned long startIndex;

		if(count >= Parameter::FILTER_COUNT_LEVEL_1)
		{
			startIndex = count - Parameter::FILTER_COUNT_LEVEL_1;
			count = Parameter::FILTER_COUNT_LEVEL_1;
		}
		else
		{
			startIndex = 0;
		}

		if(startIndex + count <= static_cast<unsigned long>(BlockList.size()))	// avoid range overflow
		{
//			Detail.InitStatistic();
			InitBlockList				(startIndex, count);
			DisableDeviatingBlocks		(startIndex, count);
			GetEquation					(startIndex, count);
			GetInterpolation();

			Detail.WriteDetail			(startIndex, count,
										 Parameter::START_UP_IGNORE_COUNT + BlockList.size()
										 );
		}
		else
		{
			QTextStream(stdout) << "BlockFilterLevel1::Add: startIndex=" << startIndex << " count=" << count << " size=" << BlockList.size() << endl;
			QTextStream(stdout).flush();
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::GetCurrent(BlockFilterDetail::Level1OutputT& output)
{
	output.AltVelo	= Result.AltVelo;
	output.Valid	= Result.Valid;
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::InitBlockList(unsigned long startIndex, unsigned long count)
{
	for(unsigned long i = startIndex; i < startIndex + count; i++)
	{
		BlockList[i].Init();
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::DisableDeviatingBlocks(unsigned long startIndex, unsigned long count)
{
	GetCount(BlockList.size());

	while(		MaxLoopCount > 0
			&&	DisableCount > 0
		)
	{
		GetEquation			(startIndex, count);
		GetDeviationFromEq	(startIndex, count);
		GetMaxDeviations	(startIndex, count);
		DisableMaxDeviations();								// decrements DisableCount by 1...5 (member count of MaxDeviationsT)

		MaxLoopCount --;
//		Detail.CollectStatistic1(startIndex, count);
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::GetEquation(unsigned long startIndex, unsigned long count)
{
	Equations.Valid = false;

	Equations.AltVelo.Init();

	unsigned long end = startIndex + count;
	unsigned long i;
	unsigned long averageCount = 0;

	for(i = startIndex; i < end; i++)
	{
		if(BlockList[i].Enabled)
		{
			double time = FilteredBlock.BlockList[i].Block.RelativeTime;

			GetEquation1(Equations.AltVelo, time, BlockList[i].AltVelo);

			Equations.LastTime = time;
			averageCount ++;
		}
	}

	if(averageCount > 0)								// avoid division by zero
	{
		Equations.Valid = GetEquation2(Equations.AltVelo, averageCount);
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::GetDeviationFromEq(unsigned long startIndex, unsigned long count)
{
	if(Equations.Valid)
	{
		for(unsigned long i = startIndex; i < startIndex + count; i++)
		{
			if(BlockList[i].Enabled)
			{
				BlockList[i].Deviation = DeviationFromEq(FilteredBlock.BlockList[i].Block.RelativeTime, Equations.AltVelo, BlockList[i].AltVelo);

				BlockList[i].DeviationValid = true;
			}
			else
			{
				BlockList[i].DeviationValid = false;
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::GetMaxDeviations(unsigned long startIndex, unsigned long count)
{
	MaxDeviations.Init();

	for(unsigned long i = startIndex; i < startIndex + count; i++)
	{
		if(		BlockList[i].Enabled
			&&	BlockList[i].DeviationValid
			)
		{
			GetMaxDeviationsSub(i, BlockList[i].Deviation, MaxDeviations.AltVelo);
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::DisableMaxDeviations()
{
	DisableMaxDeviationsSub(MaxDeviations.AltVelo);
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::DisableMaxDeviationsSub(BlockFilterDetail::MaxDeviationT& maxDeviation)
{
	if(maxDeviation.Enabled)
	{
		unsigned long i = maxDeviation.Index;

		if(i < static_cast<unsigned long>(BlockList.size()))
		{
			if(		BlockList[i].Enabled
				&&	DisableCount > 0						// don't disable more than required
				)
			{
				BlockList[i].Enabled = false;
				DisableCount --;
			}
		}
		else
		{
			QTextStream(stdout) << "BlockFilterLevel1::DisableMaxDeviationsSub: index=" << i << " size=" << BlockList.size() << endl;
			QTextStream(stdout).flush();
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BlockFilterLevel1::GetInterpolation()
{
	if(Equations.Valid)
	{
		Result.AltVelo	= InterpolationY(Equations.AltVelo, Equations.LastTime);
		Result.Valid	= true;
	}
	else
	{
		Result.Valid	= false;
	}
}
