#ifndef CsvWriter_H
#define CsvWriter_H

#include <QTextStream>
#include <QFile>

class CsvWriter
{
	// constants ----------------------------------------------------------------------------------
	public:
		static const long	INIT_LONG_FOR_MINIMUM;
		static const long	INIT_LONG_FOR_MAXIMUM;
		static const double	INIT_DOUBLE_FOR_MINIMUM;
		static const double	INIT_DOUBLE_FOR_MAXIMUM;

	private:
		static const QString	HEADER_BASIC;
		static const QString	HEADER_LEVEL_0;
		static const QString	HEADER_LEVEL_1;
		static const QString	HEADER_LEVEL_2;
		static const QString	SEPARATOR;

	// types --------------------------------------------------------------------------------------
	public:
		class MinMaxLong
		{
			public:
				long	Min;
				long	Max;
			public:
				MinMaxLong()
				{
					Init();
				}
				void Init()
				{
					Min = CsvWriter::INIT_LONG_FOR_MINIMUM;
					Max = CsvWriter::INIT_LONG_FOR_MAXIMUM;
				}
				void SetMinMax(long value)
				{
					if(value < Min)	{ Min = value; }
					if(value > Max)	{ Max = value; }
				}
		};

		class MinMaxDouble
		{
			public:
				double	Min;
				double	Max;
			public:
				MinMaxDouble()
				{
					Init();
				}
				void Init()
				{
					Min = CsvWriter::INIT_DOUBLE_FOR_MINIMUM;
					Max = CsvWriter::INIT_DOUBLE_FOR_MAXIMUM;
				}
				void SetMinMax(double value)
				{
					if(value < Min)	{ Min = value; }
					if(value > Max)	{ Max = value; }
				}
		};

	// variables ----------------------------------------------------------------------------------
	private:
		QTextStream		CsvStream;
		unsigned long	CsvMode;
		bool			StartOfLine;
		bool			Enabled;

	// functions ----------------------------------------------------------------------------------
	public:
		CsvWriter(QFile& csvFile, unsigned long csvMode);
		virtual ~CsvWriter();

		void AddElement(QString	element);
		void AddElement(double	element);
		void AddElement(long	element);
		void AddElement(MinMaxLong&		element);
		void AddElement(MinMaxDouble&	element);
		void EndLine();
};

#endif
