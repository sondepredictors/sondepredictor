#ifndef Nmea_H
#define Nmea_H

#include <iostream>
using std::endl;
#include <QString>
#include <QFile>
#include <QDateTime>

#include <Position.h>
#include <QSerialPort>
#include <QTextStream>


class Nmea
{
	// constants ----------------------------------------------------------------------------------
	public:
		enum GPS_OK
		{
			GPS_NO_DEVICE,
			GPS_INVALID,
			GPS_VALID,
			GPS_VALID_NEW_POSITION
		};

	// variables ----------------------------------------------------------------------------------
	public:
		QSerialPort&	SerialPort;
		GPS_OK			GpsState;

	private:
		QString			Line;

	// functions ----------------------------------------------------------------------------------
	public:
		Nmea(QSerialPort& serialPort);
		void ReceivePosition
			(
				Position&	position
			);
		void SendPosition
			(
				Position	position
			);

	private:
		void EvaluateLine(Position& position);
};

#endif

