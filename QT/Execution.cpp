#include <QTimer>
#include <windows.h>										// Sleep
#include <QtCore/qmath.h>
#include <QFileInfo>

#include "Common.h"
#include "Execution.h"
#include "Version.h"
#include "Parameter.h"
#include "LivePrediction.h"


const QString	Execution::SEPARATOR	= "\\";
const QString	Execution::LIVE_PREDICTION_RESULT_FILE_NAME_EXTENSION	= ".txt";
const QString	Execution::LIVE_PREDICTION_GPX_FILE_NAME_EXTENSION		= ".gpx";
//const QString	Execution::LIVE_PREDICTION_KML_FILE_NAME_EXTENSION		= ".kml";
const QString	Execution::LIVE_PREDICTION_KML_FILE_NAME				= "LivePrediction_Current.kml";
const QString	Execution::LIVE_PREDICTION_FILE_NAME_PREFIX				= "LivePrediction_";
const QString	Execution::DETAIL_FILE_NAME								= "Detail.csv";


//-------------------------------------------------------------------------------------------------
Execution::Execution(QObject *parent)
:	QObject(parent)
,	GpsRead(NmeaInPort)
,	GroundTrackFileFormat(GroundTrackFormat::FORMAT_NOT_DETECTED)
{
	Live				= NULL;
	StatusOk			= true;
	NmeaOutputApplied	= false;
	FileFormat			= GroundTrackFormat::FORMAT_NOT_DETECTED;
}

//-------------------------------------------------------------------------------------------------
Execution::~Execution()
{
}

//-------------------------------------------------------------------------------------------------
void Execution::WriteLine()
{
	Live->ResultStream << "---------------------------------------------------------" << endl;
}

//-------------------------------------------------------------------------------------------------
void Execution::OpenFile
	(
		const char*				errorText,
		const QString&			fileName,
		QIODevice::OpenModeFlag	fileMode,
		QFile&					file
	)
{
	if(!file.isOpen())
	{
		file.setFileName(fileName);
		file.open(fileMode | QIODevice::Text);
		if(!file.isOpen())
		{
			emit HideError(false);
			emit ErrorText2(errorText + fileName);
		}
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::CloseFile(QFile& file)
{
	if(file.isOpen())
	{
		file.close();
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::CreateNmeaInput(QString& nmeaInputName)
{
	NmeaInputApplied = false;

	if(nmeaInputName != NULL)
	{
		if(nmeaInputName[0] != 0)
		{
			NmeaInputApplied = true;
		}
	}

	if(NmeaInputApplied)
	{
		NmeaInputName = nmeaInputName;

		NmeaInPort.setPortName		(NmeaInputName);
		NmeaInPort.setBaudRate		(QSerialPort::Baud4800);
		NmeaInPort.setDataBits		(QSerialPort::Data8);
		NmeaInPort.setParity		(QSerialPort::NoParity);
		NmeaInPort.setStopBits		(QSerialPort::OneStop);
		NmeaInPort.setFlowControl	(QSerialPort::NoFlowControl);

		NmeaInPort.open(QIODevice::ReadWrite);
		if(!NmeaInPort.isOpen())
		{
			emit HideError(false);
			emit ErrorText5("GPS input not accessible: " + NmeaInputName);
		}
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::CreateNmeaOutput(QString& nmeaOutputName)
{
	NmeaOutputApplied = false;

	if(nmeaOutputName != NULL)
	{
		if(nmeaOutputName[0] != 0)
		{
			NmeaOutputApplied = true;
		}
	}

	if(NmeaOutputApplied)
	{
		NmeaOutputName = nmeaOutputName;

		NmeaOutPort.setPortName		(NmeaOutputName);
		NmeaOutPort.setBaudRate		(QSerialPort::Baud4800);
		NmeaOutPort.setDataBits		(QSerialPort::Data8);
		NmeaOutPort.setParity		(QSerialPort::NoParity);
		NmeaOutPort.setStopBits		(QSerialPort::OneStop);
		NmeaOutPort.setFlowControl	(QSerialPort::NoFlowControl);

		NmeaOutPort.open(QIODevice::ReadWrite);
		if(!NmeaOutPort.isOpen())
		{
			emit HideError(false);
			emit ErrorText3("Prediction output not accessible: " + NmeaOutputName);
		}
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::CreatePredictionFileName (QString& directory, QString& prediction)
{
	directory.remove("\"");

	QDirIterator	dirIt(directory, QDirIterator::NoIteratorFlags);
	QDateTime		dateTime;
	QString			fileName;

	dateTime.setTime_t(0);									// initialization for comparision

	while(dirIt.hasNext())
	{
		dirIt.next();
		QFileInfo fileInfo(dirIt.filePath());

		if(		fileInfo.isFile()
			&&	(	fileInfo.fileName().contains("output.csv")
				||	fileInfo.fileName().contains("flight_path.csv")
				)
			)
		{
			if(fileInfo.lastModified() > dateTime)
			{
				dateTime = fileInfo.lastModified();
				fileName = fileInfo.fileName();
			}
		}
	}

	prediction = fileName;
}

//-------------------------------------------------------------------------------------------------
bool Execution::CreateGroundTrackFileName(QString& directory, QString& groundTrack)
{
	QDateTime	dateTime;

	dateTime.setTime_t(0);									// initialization for comparision

	if(groundTrack.isEmpty())
	{
		QDirIterator	dirIt(directory, QDirIterator::NoIteratorFlags);

		while(dirIt.hasNext())
		{
			dirIt.next();
			QFileInfo fileInfo(dirIt.filePath());
			GroundTrackFormat::Format	fileFormat = GroundTrackFileFormat.DetectFormat(fileInfo);

			if(		fileFormat != GroundTrackFormat::FORMAT_NOT_DETECTED
				&&	fileFormat != GroundTrackFormat::FORMAT_UNKNOWN
				)
			{
				if(fileInfo.lastModified() > dateTime)
				{
					dateTime = fileInfo.lastModified();
					groundTrack = fileInfo.fileName();
					FileFormat = GroundTrackFileFormat.Type();
					GroundTrackPrefix = GroundTrackFileFormat.GroundTrackPrefix();
				}
			}
		}
	}

	return !groundTrack.isEmpty();
}

//-------------------------------------------------------------------------------------------------
void Execution::CreateLivePredictionFileName
	(
		QString&	prediction,
		QString&	groundTrack,
		QString&	livePrediction,
		const QString&	extension
	)
{
	if(!groundTrack.isEmpty())								// valid name
	{
		(void) prediction;
	//	QFileInfo fileInfoPrediction (prediction);
		QFileInfo fileInfoGroundTrack(groundTrack);

		livePrediction = fileInfoGroundTrack.baseName() + extension;
		livePrediction.insert(0, LIVE_PREDICTION_FILE_NAME_PREFIX);
		livePrediction.replace(GroundTrackPrefix, "");
		livePrediction.replace(":", "-");
	}
}

//-------------------------------------------------------------------------------------------------
bool Execution::OpenAllFiles()
{
	bool found = false;

	QString directory;										// directory name
	QString prediction;										// prediction file name
	QString groundTrack;									// ground track file name
	QString livePrediction;									// live prediction result file name
	QString livePredictionGpx;								// live prediction GPX file name
	QString livePredictionKml;								// live prediction KML file name
	QString fullPrediction;
	QString detail;

	CreateNmeaInput(	Config.Parameter.GpsNmeaInputName);
	CreateNmeaOutput(	Config.Parameter.PredictionNmeaOutputName);
	directory =			Config.Parameter.DirectoryName;

	CreatePredictionFileName			(directory, prediction);
	fullPrediction = OpenPrediction		(directory, prediction);
	found = CreateGroundTrackFileName	(directory, groundTrack);

	if(found)
	{
		QString fullGroundTrack;
		if(groundTrack.isEmpty())
		{
			emit HideError(false);
			emit ErrorText9("No ground track file (internal)");
		}
		else
		{
			fullGroundTrack = directory + SEPARATOR + groundTrack;
			OpenFile
				(	"Ground track file error: ",
					fullGroundTrack,	QIODevice::ReadOnly,	GroundTrackFile);

			CreateLivePredictionFileName(fullPrediction, fullGroundTrack, livePrediction,	 LIVE_PREDICTION_RESULT_FILE_NAME_EXTENSION);
			CreateLivePredictionFileName(fullPrediction, fullGroundTrack, livePredictionGpx, LIVE_PREDICTION_GPX_FILE_NAME_EXTENSION);
	//		CreateLivePredictionFileName(fullPrediction, fullGroundTrack, livePredictionKml, LIVE_PREDICTION_KML_FILE_NAME_EXTENSION);
			livePredictionKml	= LIVE_PREDICTION_KML_FILE_NAME;
			detail				= DETAIL_FILE_NAME;
		}

		if(livePrediction.isEmpty() || livePredictionGpx.isEmpty() || livePredictionKml.isEmpty())
		{
			emit HideError(false);
			emit ErrorText6("No live prediction result file");
		}
		else
		{
			QString fullLivePrediction = directory + SEPARATOR + livePrediction;
			QFile::remove(fullLivePrediction);
			OpenFile
				(	"Live prediction file error: ",
					fullLivePrediction,	QIODevice::WriteOnly, LivePredictionFile);

			QString fullLivePredictionGpx = directory + SEPARATOR + livePredictionGpx;
			QFile::remove(fullLivePredictionGpx);
			OpenFile
				(	"Live prediction GPX file error: ",
					fullLivePredictionGpx,	QIODevice::WriteOnly, LivePredictionGpxFile);

			QString fullLivePredictionKml = directory + SEPARATOR + livePredictionKml;
			QFile::remove(fullLivePredictionKml);
			OpenFile
				(	"Live prediction KML file error: ",
					fullLivePredictionKml,	QIODevice::WriteOnly, LivePredictionKmlFile);

			QString fullDetail = directory + SEPARATOR + detail;
			QFile::remove(fullDetail);
			if(Config.Parameter.Detail != BlockFilterDetail::DETAIL_MODE_OFF)
			{
				OpenFile
					(	"Detail file error: ",
						fullDetail,	QIODevice::WriteOnly, DetailFile);
			}
		}
	}

	return found;
}

//-------------------------------------------------------------------------------------------------
QString Execution::OpenPrediction(QString directory, QString fileName)
{
	QString fullPrediction;

	if(fileName.isEmpty())
	{
		emit HideError(false);
		emit ErrorText7("No CUSF prediction file");
	}
	else
	{
		fullPrediction = directory + SEPARATOR + fileName;
		OpenFile
			(	"CUSF prediction file error: ",
				fullPrediction,		QIODevice::ReadOnly,	PredictionFile);
	}

	StatusOk = PredictionFile.isOpen() && PredictionFile.isReadable();

	return fullPrediction;
}

//-------------------------------------------------------------------------------------------------
bool Execution::AllFilesOpen()
{
	return	PredictionFile.isOpen()
		&&	GroundTrackFile.isOpen()
		&&	LivePredictionFile.isOpen()
		&&	LivePredictionGpxFile.isOpen()
		&&	LivePredictionKmlFile.isOpen();
//		&&	DetailFile.isOpen();							// may be not opened
}

//-------------------------------------------------------------------------------------------------
void Execution::CloseAllFiles()
{
	CloseFile(PredictionFile);
	CloseFile(GroundTrackFile);
	CloseFile(LivePredictionFile);
	CloseFile(LivePredictionGpxFile);
	CloseFile(LivePredictionKmlFile);
	if(DetailFile.isOpen())
	{
		CloseFile(DetailFile);
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::WriteHeader()
{
	Live->ResultStream << "SondePredictor " << Version::VERSION << " by bitbucket.org/sondepredictors" << endl;
}

//-------------------------------------------------------------------------------------------------
void Execution::WriteFilenames()
{
	QFileInfo config(Configuration::CONFIGURATION_FILE_NAME);
	Live->ResultStream	<< "Path: " << config.absolutePath() << endl;
	Live->ResultStream	<< "Configuration file:                  " << Configuration::CONFIGURATION_FILE_NAME << endl;

	if(LivePredictionFile.isOpen())
	{
		if(PredictionFile.isOpen())
		{
			Live->ResultStream	<< "File from CUSF (predict.habhub.org): " << PredictionFile.fileName() << endl;
		}

		if(GroundTrackFile.isOpen())
		{
			Live->ResultStream	<< "Sondemonitor ground track file:      " << GroundTrackFile.fileName() << endl;
		}

		Live->ResultStream		<< "Live prediction result file (this):  " << LivePredictionFile.fileName() << endl;

		if(LivePredictionGpxFile.isOpen())
		{
			Live->ResultStream	<< "Live prediction GPX file:            " << LivePredictionGpxFile.fileName() << endl;
		}

		if(LivePredictionKmlFile.isOpen())
		{
			Live->ResultStream	<< "Live prediction KML file:            " << LivePredictionKmlFile.fileName() << endl;
		}

		if(DetailFile.isOpen())
		{
			Live->ResultStream	<< "Detail file:                         " << DetailFile.fileName() << endl;
		}
	}

	if(NmeaInputApplied)
	{
		if(NmeaInPort.isOpen())
		{
			Live->ResultStream << "GPS NMEA input: " << NmeaInputName << endl;
		}
		else
		{
			Live->ResultStream << "GPS NMEA input not accessible: " << NmeaInputName << endl;
		}
	}
	else
	{
		Live->ResultStream << "GPS NMEA input not applied" << endl;
	}

	if(NmeaOutputApplied)
	{
		if(NmeaOutPort.isOpen())
		{
			Live->ResultStream << "Prediction NMEA output: " << NmeaOutputName << endl;
		}
		else
		{
			Live->ResultStream << "Prediction NMEA output not accessible: " << NmeaOutputName << endl;
		}
	}
	else
	{
		Live->ResultStream << "Prediction NMEA output not applied" << endl;
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::PrepareUi()
{
	emit HideNormal(true);
	emit HideError (true);

	emit GpsOk(Nmea::GPS_NO_DEVICE);

	emit SondeDistance	("---");
	emit SondeDirection	("---");
	emit Distance		("---");
	emit Direction		("---");

	emit SondeLanding	("Sonde");
	emit Altitude		("---");
	emit AscentDescent	("Waiting");
	emit Duration		("---");
	emit Latitude		("");
	emit LatitudeSign	("");
	emit Longitude		("");
	emit LongitudeSign	("");
	emit Rate			("---");
	emit RatePercent	("---");

	emit BlocksGood		("---");
	emit BlocksGood		("---");

	emit ErrorText1("Error");
	emit ErrorText2("");
	emit ErrorText3("");
	emit ErrorText4("");
	emit ErrorText5("");
	emit ErrorText6("");
	emit ErrorText7("");
	emit ErrorText8("");
	emit ErrorText9("");
}

//-------------------------------------------------------------------------------------------------
void Execution::Start()
{
	PrepareUi();
	Config.HandleDefaultFile();
	Config.ReadParameters();
	Start1();
}

//-------------------------------------------------------------------------------------------------
void Execution::Start1()
{
	if(OpenAllFiles())
	{
		Start2();
	}
	else
	{
		emit HideError(false);
		emit ErrorText1("Waiting for ground track file...");
		QTimer::singleShot(1000 * Parameter::PAUSE_DURATION, this, SLOT(Start1()));
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::Start2()
{
	if(AllFilesOpen())
	{
		GroundTrackFile.close();							// normaly closed, will be opened for reading

		Live = new LivePrediction
			(
				this,
				PredictionFile,
				GroundTrackFile,
				FileFormat,
				LivePredictionFile,
				LivePredictionGpxFile,
				LivePredictionKmlFile,
				DetailFile,
				Config.Parameter.Detail,
				NmeaOutPort
			);

		if(Live != NULL)
		{
			WriteHeader();
			WriteLine();
			WriteFilenames();
			WriteLine();

			emit HideNormal(false);
			if(Live->Prepare())
			{
				ShowGroundTrackMode();
				emit HideError(true);
				Work();
				QObject::connect(&NmeaInPort, SIGNAL(readyRead()), &GpsRead, SLOT(PositionUpdated()));
				QObject::connect(&GpsRead, SIGNAL(GpsUpdate(Nmea::GPS_OK, double, double)), this, SLOT(GpsUpdate(Nmea::GPS_OK, double, double)));
			}
			else
			{
				emit HideNormal(true);
				emit HideError (false);
			}
		}
		else
		{
			emit HideError(false);
			emit ErrorText9("Out of memory (internal)");
		}
	}
	else
	{
		emit HideError(false);
		emit ErrorText8("Not all files available");
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::Work()
{
	if(Live->Work())
	{
		QTimer::singleShot(0, this, SLOT(Work()));
	}
	else
	{
		QTimer::singleShot(1000 * Parameter::PAUSE_DURATION, this, SLOT(Work()));
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::ShowGroundTrackMode()
{
	QString lineFormat = GroundTrackFileFormat.FormatName(FileFormat);

	if(!lineFormat.isEmpty())
	{
		Live->ResultStream << "Detected ground track file sonde type: " << lineFormat << endl;
		Live->ResultStream << "--------------------------------------------------------" << endl;
		Live->ResultStream << endl;
		Live->ResultStream << "LivePrediction:" << endl;
		Live->ResultStream.flush();
	}
}

//-------------------------------------------------------------------------------------------------
void Execution::GpsUpdate(Nmea::GPS_OK gpsOk, double latitude, double longitude)
{
//	bool landing =		Live->SondeTrack.CurrentPhase == Position::PHASE_DESCENT
//					&&	Live->LandingPosition.Valid;
	double distance;
	double direction;

	if(gpsOk == Nmea::GPS_VALID_NEW_POSITION)
	{
		if(Live->LandingPosition.Valid)
		{
			CalculateVector										// direction to landing position
					(	latitude,
						longitude,
						Live->LandingPosition.Latitude,
						Live->LandingPosition.Longitude,
						distance, direction
					);

			emit Distance	(QString::number(distance / 1000, 'g', 3));
			emit Direction	(QString::number(lround(direction)));
		}

		if(gpsOk && Live->CurrentPosition.Valid)
		{
			CalculateVector										// direction to sonde
					(	latitude,
						longitude,
						Live->CurrentPosition.Latitude,
						Live->CurrentPosition.Longitude,
						distance, direction
					);

			emit SondeDistance	(QString::number(distance / 1000, 'g', 3));
			emit SondeDirection	(QString::number(lround(direction)));
		}
	}

	emit GpsOk(gpsOk);
}

//-------------------------------------------------------------------------------------------------
void Execution::CalculateVector
	(	double lat1, double lon1,
		double lat2, double lon2,
		double& distance, double& direction
	)
{
	double latitudeMeter;
	double longitudeMeter;
	Position::DegreeToDeltaMeter
		(	lat2, lat1,
			lon2, lon1,
			latitudeMeter, longitudeMeter
		);
	distance = qSqrt(latitudeMeter*latitudeMeter + longitudeMeter*longitudeMeter);

	direction = qAtan2(longitudeMeter, latitudeMeter) * 57.2957795;
	if(direction < 0)
	{
		direction = direction + 360.;
	}
}
