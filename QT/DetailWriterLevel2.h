#ifndef DetailWriterLevel2_H
#define DetailWriterLevel2_H

#include "CsvWriter.h"
#include "BlockFilterDetail.h"

class BlockFilterLevel1;


class DetailWriterLevel2
{
	// constants ----------------------------------------------------------------------------------
	private:

	// types --------------------------------------------------------------------------------------
	private:

	// variables ----------------------------------------------------------------------------------
	private:
		CsvWriter&								Csv;
		unsigned long							CsvMode;
		BlockFilterDetail::BlockListLevel2T&	BlockList;
//		BlockFilterLevel0&						FilteredBlock;
		BlockFilterDetail::Level2OutputT&		Interpolation;

	// functions ----------------------------------------------------------------------------------
	public:
		DetailWriterLevel2
			(
				CsvWriter&								csv,
				unsigned long							csvMode,
				BlockFilterDetail::BlockListLevel2T&	blockList,
//				BlockFilterLevel0&						filteredBlock,
				BlockFilterDetail::Level2OutputT&		interpolation
			);
		virtual ~DetailWriterLevel2();

		void	WriteDetail
			(
				unsigned long	startIndex,
				unsigned long	count,
				long			lineNumber
			);

	private:
};

#endif
