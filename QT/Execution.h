#ifndef Execution_H
#define Execution_H

#include <iostream>
using std::endl;
#include <QObject>
#include <QString>
#include <QFile>
#include <QDirIterator>
#include <QDateTime>
#include <QSerialPort>

#include "Configuration.h"
#include "Position.h"
#include "GpsReader.h"
#include "GroundTrackFormat.h"

class LivePrediction;


class Execution : public QObject
{
	Q_OBJECT

	// constants ----------------------------------------------------------------------------------
	public:
		static const QString	SEPARATOR;

	private:
		static const QString	LIVE_PREDICTION_RESULT_FILE_NAME_EXTENSION;
		static const QString	LIVE_PREDICTION_GPX_FILE_NAME_EXTENSION;
//		static const QString	LIVE_PREDICTION_KML_FILE_NAME_EXTENSION;
		static const QString	LIVE_PREDICTION_KML_FILE_NAME;
		static const QString	LIVE_PREDICTION_FILE_NAME_PREFIX;
		static const QString	DETAIL_FILE_NAME;

	// variables ----------------------------------------------------------------------------------
	public:
		QFile			PredictionFile;
		QFile			GroundTrackFile;
		QFile			LivePredictionFile;
		QFile			LivePredictionGpxFile;
		QFile			LivePredictionKmlFile;
		QFile			DetailFile;
		QSerialPort		NmeaOutPort;						// live prediction output
		QSerialPort		NmeaInPort;							// GPS position input
		LivePrediction*	Live;
		bool			StatusOk;
		GpsReader		GpsRead;

	private:
		QString	GroundTrackPrefix;
		QString NmeaInputName;
		bool	NmeaInputApplied;
		QString NmeaOutputName;
		bool	NmeaOutputApplied;
		Configuration				Config;
		GroundTrackFormat			GroundTrackFileFormat;
		GroundTrackFormat::Format	FileFormat;

	// functions ----------------------------------------------------------------------------------
	public:
		Execution(QObject *parent = 0);
		virtual ~Execution();

		void Start();

	private:
		void Start2();
		void WriteLine();
		void OpenFile
			(
				const char*				errorText,
				const QString&			fileName,
				QIODevice::OpenModeFlag	fileMode,
				QFile&					file
			);
		void CloseFile
			(
				QFile& file
			);
		void CreateNmeaInput					// initiate and open NmeaPort
			(
				QString &nmeaInputName			// in:	name of NMEA output, e.g. COM4
			);
		void CreateNmeaOutput					// initiate and open NmeaPort
			(
				QString &nmeaOutputName			// in:	name of NMEA output, e.g. COM5
			);
		void CreatePredictionFileName
			(
				QString& directory,
				QString& prediction
			);
		bool CreateGroundTrackFileName
			(
				QString& directory,
				QString& groundTrack
			);
		void CreateLivePredictionFileName
			(
				QString& prediction,
				QString& groundTrack,
				QString& livePrediction,
				const QString& extension
			);
		bool OpenAllFiles();
		bool AllFilesOpen();
		void CloseAllFiles();
		void WriteHeader();
		void WriteFilenames();
		QString OpenPrediction
			(
				QString	directory,
				QString	fileName
			);
		void PrepareUi();
		void CalculateVector
			(	double lat1, double lon1,
				double lat2, double lon2,
				double& distance, double& direction
			);
		void ShowGroundTrackMode();

	// signals, slots
	signals:
		void Phase			(const Position::Phase	phase);
		void GpsOk			(const Nmea::GPS_OK		gpsOk);

		void HideNormal		(const bool		hide);
		void HideError		(const bool		hide);

		void SondeDistance	(const QString	distance);
		void SondeDirection	(const QString	direction);
		void Distance		(const QString	distance);
		void Direction		(const QString	direction);

		void SondeLanding	(const QString	text);
		void Altitude		(const QString	text);
		void AscentDescent	(const QString	text);
		void Duration		(const QString	text);
		void Latitude		(const QString	text);
		void LatitudeSign	(const QString	text);
		void Longitude		(const QString	text);
		void LongitudeSign	(const QString	text);
		void Rate			(const QString	text);
		void RatePercent	(const QString	text);

		void BlocksGood		(const QString	text);
		void BlocksBad		(const QString	text);

		void ErrorText1	(const QString	text);
		void ErrorText2	(const QString	text);
		void ErrorText3	(const QString	text);
		void ErrorText4	(const QString	text);
		void ErrorText5	(const QString	text);
		void ErrorText6	(const QString	text);
		void ErrorText7	(const QString	text);
		void ErrorText8	(const QString	text);
		void ErrorText9	(const QString	text);

	public slots:
		void GpsUpdate(Nmea::GPS_OK gpsOk, double latitude, double longitude);

	private slots:
		void Start1();
		void Work();
};

#endif

