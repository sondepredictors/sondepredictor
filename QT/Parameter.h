#ifndef Parameter_H
#define Parameter_H

#include "XShiftFilter.h"


class Parameter
{
	// types --------------------------------------------------------------------------------------
	public:

	// constants ----------------------------------------------------------------------------------
	public:
		static const double			MIN_TIME;							// min. time [s] of groundtrack file before processing starts
		static const unsigned long	PAUSE_DURATION;						// loop pause duration [s]
		static const double			MIN_V_ALT_RATIO_SLOW;				// minimum of altitude velocity ratio
		static const double			ALTITUDE_ADAPTION;					// [m] altitude delta before TimeCorrection starts
		static const unsigned long	START_UP_IGNORE_COUNT;				// number of ignored values during start up

		static const double			PLAUSI_MIN_TIME;					// min. possible time [s]
		static const double			PLAUSI_MAX_TIME;					//
		static const double			PLAUSI_MIN_LATITUDE;				// min. possible latitude [°]
		static const double			PLAUSI_MAX_LATITUDE;				//
		static const double			PLAUSI_MIN_LONGITUDE;				// lowest possible longitude [°]
		static const double			PLAUSI_MAX_LONGITUDE;				//
		static const double			PLAUSI_MIN_ALTITUDE;				// lowest possible altitude [m]
		static const double			PLAUSI_MAX_ALTITUDE;				//
		static const double			PLAUSI_MIN_V_ALT;					// lowest possible altidute velocity [m/s]
		static const double			PLAUSI_MAX_V_ALT;					//
#ifdef USE_BLOCK_FILTER
		static const double			PHASE_CHANGE_DELAY;					// delay [s] after burst to get reliable altitude velocity
		static const unsigned long	MIN_COUNT;							// min. number of entries for valid result
		static const unsigned long	FILTER_COUNT;						// number of filter entries: general, level 0
		static const unsigned long	FILTER_COUNT_LEVEL_1;				// number of filter entries: level 1
		static const unsigned long	FILTER_COUNT_LEVEL_2;				// number of filter entries: level 2
		static const float			DISABLE_RATE;						// ratio of discarded positions from total
		static const float			TIME_DISABLE_RATE;					// ratio of discarded positions from total for time
#else
		static const double			V_ALT_RATIO_SLOW		= 0.7;		// altitude velocity ratio for TimeCorrection, slow descent
		static const double			V_ALT_RATIO_FAST		= 1.4;		// altitude velocity ratio for TimeCorrection, fast descent

		static const double			PHASE_CHANGE_ALT_DELTA	= 200;		// max. expected difference [m] of lat/lon/height
		static const double			COMPARE_PT1_FACTOR		= 0.1;		// filter for compare value to discard inexact values
		static const unsigned long	START_UP_AVERAGE_COUNT	= 33;		// number of compare values for start up average, including START_UP_IGNORE_COUNT
		static const double			TIME_TOLERANCE			= 5.1;		// [s]
		static const double			ALTITUDE_TOLERANCE		= 100;		// [m]
		static const double			ALT_VELO_TOLERANCE		= 100;		// [m/s]
		static const double			LAT_LON_TOLERANCE		= 100;		// [m]
		static const double			ROUGHLY_LATITUDE		= 51.5;		// [°]

		static const XShiftFilter::ConfigurationT	ALT_VELO_FILTER;
		static const XShiftFilter::ConfigurationT	ALT_VELO_RATIO_X_FILTER [4];
#endif

	// variables ----------------------------------------------------------------------------------
	public:
#ifdef USE_BLOCK_FILTER
#else
		double LatTolerance;													// [°]
		double LonTolerance;													// [°]
#endif

	// functions ----------------------------------------------------------------------------------
	public:
		Parameter();
};

#endif
