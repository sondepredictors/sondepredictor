#ifndef GroundTrackFormat_H
#define GroundTrackFormat_H

#include <QObject>
#include <QFileInfo>

#include "Position.h"


class GroundTrackFormat : public QObject
{
	Q_OBJECT

	// constants ----------------------------------------------------------------------------------
	public:
		enum Format
		{
			FORMAT_NOT_DETECTED,
			FORMAT_UNKNOWN,
			FORMAT_DFM06,				// date, time,	one line
			FORMAT_RS92,				// seconds,		one line
			FORMAT_M10,					// day time,	one line
			FORMAT_C34					// date, time,	several lines
		};

	private:
		static const QString	GROUND_TRACK_FILE_NAME_PREFIX_RS92;
		static const QString	GROUND_TRACK_FILE_NAME_PREFIX_M10;
		static const QString	GROUND_TRACK_FILE_NAME_PREFIX_DFM06;	// DFM06, DFM09
		static const QString	GROUND_TRACK_FILE_NAME_PREFIX_C34;

		static const QString	GROUND_TRACK_FILE_NAME_SUFFIX_RS92;
		static const QString	GROUND_TRACK_FILE_NAME_SUFFIX_M10;
		static const QString	GROUND_TRACK_FILE_NAME_SUFFIX_DFM06;
		static const QString	GROUND_TRACK_FILE_NAME_SUFFIX_C34;

//		static const QString	GROUND_TRACK_FILE_NAME_MARKER_RS92;		// not available
		static const QString	GROUND_TRACK_FILE_NAME_MARKER_M10;
		static const QString	GROUND_TRACK_FILE_NAME_MARKER_DFM06;
		static const QString	GROUND_TRACK_FILE_NAME_MARKER_C34;

		static const QString	NAME_DFM06;
		static const QString	NAME_RS92;
		static const QString	NAME_M10;
		static const QString	NAME_C34;

		static const QString	SEPARATOR_COMMA;
		static const QString	SEPARATOR_BLANC;
		static const long		INVALID_TIME	= -1;

		enum BlockStateT				// sequence of incoming data
		{
			BLOCK_WAIT_DATE,
			BLOCK_WAIT_TIME,
			BLOCK_WAIT_LAT,
			BLOCK_WAIT_LON,
			BLOCK_WAIT_ALT
		};

	// types --------------------------------------------------------------------------------------
	public:
		class ContentT
		{
			public:
				QString	StartDay;
				double	StartTime;							// UNIX time of 1st valid line
				Position Pos;

			public:
				ContentT()
				{
					StartDay	= "";
					StartTime	= 0;
				}
		};

		class BlockT
		{
			public:
				BlockT()
				{
					LastKey		= 0;
					BlockState	= BLOCK_WAIT_DATE;
					Reset		= false;
				}

			public:
				unsigned long	LastKey;
				BlockStateT		BlockState;
				bool			Reset;
		};

	// variables ----------------------------------------------------------------------------------
	public:
		ContentT	Content;

	private:
		Format	FileFormat;
		QString	FilePrefix;
		BlockT	Block;					// for multi line file formats, e.g. C34
		QString	TempDate;

	// functions ----------------------------------------------------------------------------------
	public:
		GroundTrackFormat
			(	Format		inputFileFormat,
				QObject*	parent = 0
			);
		virtual ~GroundTrackFormat();

		Format	DetectFormat(QFileInfo fileInfo);
		QString GroundTrackPrefix();
		QString	FormatName(Format fileFormat);
		bool	EvaluateFirstLine(QString& text);
		bool	EvaluateLine
			(
				QString&	text,
				bool		firstLine
			);
		long	GetTimeFromDateTime
			(
				QString&	date,
				QString&	time,
				QString		format);
		long	GetTimeFromDayTime
			(
				QString&	day,
				QString&	time
			);
		bool	BlockInProgress();
		bool	BlockAborted();

		inline Format	Type()	{ return FileFormat; }

	private:
		bool	DataFieldsOk(bool firstLine);
};

#endif
