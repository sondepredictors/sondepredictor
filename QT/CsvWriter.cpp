#include <float.h>

#include "Common.h"
#include "CsvWriter.h"
#include "Version.h"
#include "BlockFilterDetail.h"


const long		CsvWriter::INIT_LONG_FOR_MINIMUM	= +LONG_MAX;
const long		CsvWriter::INIT_LONG_FOR_MAXIMUM	= -LONG_MAX;
const double	CsvWriter::INIT_DOUBLE_FOR_MINIMUM	= +DBL_MAX;
const double	CsvWriter::INIT_DOUBLE_FOR_MAXIMUM	= -DBL_MAX;


const QString	CsvWriter::HEADER_BASIC
		=	"Line; BlkI; "								// A
			"Time; Alt; Lat; Lon; "						// C
			"IpoT; IpoAlt; IpoLat; IpoLon; "			// G
			"Velo; IpoVelo; "							// K
			"VeloRatio; IpoVeloRatio; "					// M
		;

const QString	CsvWriter::HEADER_LEVEL_0
		=	"Line; BlkI; "								// A

			"Time; Altitude; "							// C

			"IMin; IMax; "								// E
			"TAvgCnt; TAvgMin; TAvgMax; "				// G
			"MaxTDevCnt; MaxTDevIMin; MaxTDevIMax;"		// J
			"MaxTDevMin; MaxTDevMax; "					// M
			"DisBlkCnt; DisBlkIMin; DisBlkIMax; "		// O
			"DisCntI; DisCnt;"							// R

			"IMin; IMax; EqCnt; LastTMin; LastTMax; "	// T
#ifdef USE_LINEAR
			"AltXmMin; AltXmMax; "						// Y
			"AltYmMin; AltYmMax; "						// AA
			"AltXvMin; AltXvMax; "						// AC
			"AltKvMin; AltKvMax; "						// AE
			"AltAMin; AltAMax; "						// AG
			"AltBMin; AltBMax; "						// AI
#else
			"AltXMin; AltXMax; "						// Y
			"AltX2Min; AltX2Max; "						// AA
			"AltX3Min; AltX3Max; "						// AC
			"AltX4Min; AltX4Max; "						// AE
			"AltYMin; AltYMax; "						// AG
			"AltYx2Min; AltYx2Max; "					// AI
			"AltXyMin; AltXyMax; "						// AK
			"AltAMin; AltAMax; "						// AM
			"AltBMin; AltBMax; "						// AO
			"AltCMin; AltCMax; "						// AQ
#endif
														// linear	square
			"AltDevMin; AltDevMax; "					// AK		AS
			"AltMaxDevIMin; AltMaxDevIMax; "			// AM		AU
			"AltMaxDevMin; AltMaxDevMax; "				// AO		AW

			"DisCnt; MaxCnt; "							// AQ		AY

#ifdef USE_LINEAR
			"ResAltXm; ResAltYm; ResAltXv; ResAltKv; "	// AS
			"ResAltA; ResAltB; "						// AW
#else
			"ResAltX; ResAltX2; ResAltX3; ResAltX4; "	//			BA
			"ResAltY; ResAltYx2; ResAltXy; "			//			BE
			"ResAltA; ResAltB; ResAltC; "				//			BH
#endif
			"IpoT; IpoAlt; "							// AY		BK
		;

const QString	CsvWriter::HEADER_LEVEL_1
		=	"Line; BlkI; "								// A
			"Time; Velo; "								// C
			"IpoT; IpoVelo; "							// E
		;

const QString	CsvWriter::HEADER_LEVEL_2
		=	"Line; BlkI; "								// A
			"PreAltVelo; "								// C
			"AVRatio; IpoAVRatio; "						// D
		;

const QString	CsvWriter::SEPARATOR	= ";";


//-------------------------------------------------------------------------------------------------
CsvWriter::CsvWriter(QFile& csvFile, unsigned long csvMode)
	: CsvMode(csvMode)
{
	if(csvFile.isOpen())
	{
		CsvStream.setDevice(&csvFile);

		switch(CsvMode)
		{
			case BlockFilterDetail::DETAIL_MODE_BASIC:
				CsvStream << HEADER_BASIC;
				break;
			case BlockFilterDetail::DETAIL_MODE_LEVEL_0:
				CsvStream << HEADER_LEVEL_0;
				break;
			case BlockFilterDetail::DETAIL_MODE_LEVEL_1:
				CsvStream << HEADER_LEVEL_1;
				break;
			case BlockFilterDetail::DETAIL_MODE_LEVEL_2:
				CsvStream << HEADER_LEVEL_2;
				break;
			default:
				break;
		}

		CsvStream << "[SondePredictor " << Version::VERSION << "]";

		Enabled = true;
		StartOfLine = true;
	}
	else
	{
		Enabled = false;
	}
}

//-------------------------------------------------------------------------------------------------
CsvWriter::~CsvWriter()
{
}

//-------------------------------------------------------------------------------------------------
void CsvWriter::AddElement(QString element)
{
	if(Enabled)
	{
		if(!StartOfLine)
		{
			CsvStream << SEPARATOR << " ";
		}
		CsvStream << element;

		StartOfLine = false;
	}
}

//-------------------------------------------------------------------------------------------------
void CsvWriter::AddElement(double element)
{
	if(Enabled)
	{
		QString elementString;
		if(element < INIT_DOUBLE_FOR_MINIMUM && element > INIT_DOUBLE_FOR_MAXIMUM)
		{
			elementString = QString::number(element);
		}
		else
		{
			elementString = "";
		}

		AddElement(elementString);
	}
}

//-------------------------------------------------------------------------------------------------
void CsvWriter::AddElement(long element)
{
	if(Enabled)
	{
		QString elementString;
		if(element < INIT_LONG_FOR_MINIMUM && element > INIT_LONG_FOR_MAXIMUM)
		{
			elementString = QString::number(element);
		}
		else
		{
			elementString = "";
		}

		AddElement(elementString);
	}
}

//-------------------------------------------------------------------------------------------------
void CsvWriter::AddElement(MinMaxLong& element)
{
	AddElement(element.Min);
	AddElement(element.Max);
}

//-------------------------------------------------------------------------------------------------
void CsvWriter::AddElement(MinMaxDouble& element)
{
	AddElement(element.Min);
	AddElement(element.Max);
}

//-------------------------------------------------------------------------------------------------
void CsvWriter::EndLine()
{
	if(Enabled)
	{
		StartOfLine = true;
		CsvStream << endl;
	}
}
