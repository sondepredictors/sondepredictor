#include "Common.h"
#include "Parameter.h"
#include "Position.h"


const double		Parameter::MIN_TIME				= 0.;
const unsigned long	Parameter::PAUSE_DURATION		= 1;
const double		Parameter::MIN_V_ALT_RATIO_SLOW	= 0.1;
const double		Parameter::ALTITUDE_ADAPTION	= 2000;
const unsigned long	Parameter::START_UP_IGNORE_COUNT= 30;

const double		Parameter::PLAUSI_MIN_TIME		= 0;
const double		Parameter::PLAUSI_MAX_TIME		= 36000;
const double		Parameter::PLAUSI_MIN_LATITUDE	= -90;
const double		Parameter::PLAUSI_MAX_LATITUDE	=  90;
const double		Parameter::PLAUSI_MIN_LONGITUDE	= -180;
const double		Parameter::PLAUSI_MAX_LONGITUDE	=  180;
const double		Parameter::PLAUSI_MIN_ALTITUDE	= 0;
const double		Parameter::PLAUSI_MAX_ALTITUDE	= 50000;
const double		Parameter::PLAUSI_MIN_V_ALT		= -50;
const double		Parameter::PLAUSI_MAX_V_ALT		=  50;

#ifdef USE_BLOCK_FILTER
const double		Parameter::PHASE_CHANGE_DELAY	= 10;
const unsigned long	Parameter::MIN_COUNT			= 10;
const unsigned long	Parameter::FILTER_COUNT			= 500;
const unsigned long	Parameter::FILTER_COUNT_LEVEL_1	= 500;
const unsigned long	Parameter::FILTER_COUNT_LEVEL_2	= 200;
const float			Parameter::DISABLE_RATE			= 0.05;
const float			Parameter::TIME_DISABLE_RATE	= 0.1;
#else
const XShiftFilter::ConfigurationT	Parameter::ALT_VELO_FILTER =
	{
		20, 20, 10
	};

const XShiftFilter::ConfigurationT	Parameter::ALT_VELO_RATIO_X_FILTER [4] =
	{
		10,		10,		5,				// 1st prediction
		20,		20,		10,				// 2nd prediction
		50,		50,		25,				// 3rd prediction
		100,	100,	50				// 4th and all following predictions
	};
#endif


//-------------------------------------------------------------------------------------------------
Parameter::Parameter()
{
#ifdef USE_BLOCK_FILTER
#else
	Position::MeterToDegree
	(
		LAT_LON_TOLERANCE,
		LAT_LON_TOLERANCE,
		ROUGHLY_LATITUDE,
		LatTolerance,										// indepentent from latitude
		LonTolerance										// depends on ROUGHLY_LATITUDE
	);
#endif
}
