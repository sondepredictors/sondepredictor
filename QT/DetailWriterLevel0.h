#ifndef DetailWriterLevel0_H
#define DetailWriterLevel0_H

#include "CsvWriter.h"
#include "BlockFilterDetail.h"


class DetailWriterLevel0
{
	// constants ----------------------------------------------------------------------------------
	private:

	// types --------------------------------------------------------------------------------------
	private:
		class TimeStatisticT
		{
			public:
				CsvWriter::MinMaxLong	Index;

				CsvWriter::MinMaxDouble	TimeAverage;
				long					TimeAverageCount;

				CsvWriter::MinMaxDouble	MaxTimeDeviation;
				CsvWriter::MinMaxLong	MaxTimeDeviationIndex;
				long					MaxTimeDeviationCount;

				CsvWriter::MinMaxLong	DisabledBlockIndex;
				long					DisabledBlockCount;

				long	DisableCountIndex;
				long	DisableCount;

			public:
				TimeStatisticT()
				{
					Init();
				}

				void Init()
				{
					Index.Init();

					TimeAverage.Init();
					TimeAverageCount	= 0;

					MaxTimeDeviation.Init();
					MaxTimeDeviationIndex.Init();
					MaxTimeDeviationCount	= 0;

					DisabledBlockIndex.Init();
					DisabledBlockCount	= 0;

					DisableCountIndex	= 0;
					DisableCount		= 0;
				}
		};

		class StatisticT
		{
			public:
				CsvWriter::MinMaxLong	Index;
				long					EquationCount;
#ifdef USE_LINEAR
				CsvWriter::MinMaxDouble	Altitude_xm;
				CsvWriter::MinMaxDouble	Altitude_ym;
				CsvWriter::MinMaxDouble	Altitude_xv;
				CsvWriter::MinMaxDouble	Altitude_kv;
				CsvWriter::MinMaxDouble	Altitude_a;
				CsvWriter::MinMaxDouble	Altitude_b;
#else
				CsvWriter::MinMaxDouble	Altitude_x;
				CsvWriter::MinMaxDouble	Altitude_x2;
				CsvWriter::MinMaxDouble	Altitude_x3;
				CsvWriter::MinMaxDouble	Altitude_x4;
				CsvWriter::MinMaxDouble	Altitude_y;
				CsvWriter::MinMaxDouble	Altitude_yx2;
				CsvWriter::MinMaxDouble	Altitude_xy;
				CsvWriter::MinMaxDouble	Altitude_a;
				CsvWriter::MinMaxDouble	Altitude_b;
				CsvWriter::MinMaxDouble	Altitude_c;
#endif
				CsvWriter::MinMaxDouble	LastTime;
				CsvWriter::MinMaxDouble	Altitude_Deviation;
				CsvWriter::MinMaxLong	Altitude_MaxDeviationIndex;
				CsvWriter::MinMaxDouble	Altitude_MaxDeviation;

			public:
				StatisticT()
				{
					Init();
				}

				void Init()
				{
					Index						.Init();
					EquationCount				= 0;
#ifdef USE_LINEAR
					Altitude_xm					.Init();
					Altitude_ym					.Init();
					Altitude_xv					.Init();
					Altitude_kv					.Init();
					Altitude_a					.Init();
					Altitude_b					.Init();
#else
					Altitude_x					.Init();
					Altitude_x2					.Init();
					Altitude_x3					.Init();
					Altitude_x4					.Init();
					Altitude_y					.Init();
					Altitude_yx2				.Init();
					Altitude_xy					.Init();
					Altitude_a					.Init();
					Altitude_b					.Init();
					Altitude_c					.Init();
#endif
					LastTime					.Init();
					Altitude_Deviation			.Init();
					Altitude_MaxDeviationIndex	.Init();
					Altitude_MaxDeviation		.Init();
				}
		};

	// variables ----------------------------------------------------------------------------------
	private:
		CsvWriter&									Csv;
		unsigned long								CsvMode;
		BlockFilterDetail::BlockListLevel0T&		BlockList;
		BlockFilterDetail::TimeAverageT&			TimeAverage;
		BlockFilterDetail::EquationsLevel0T&		Equations;
		BlockFilterDetail::MaxDeviationT&			MaxTimeDeviation;
		BlockFilterDetail::MaxDeviationsLevel0T&	MaxDeviations;
		Position&									Interpolation;
		TimeStatisticT								TimeStatistic;
		StatisticT									Statistic;

	// functions ----------------------------------------------------------------------------------
	public:
		DetailWriterLevel0
			(
				CsvWriter&									csv,
				unsigned long								csvMode,
				BlockFilterDetail::BlockListLevel0T&		blockList,
				BlockFilterDetail::TimeAverageT&			timeAverage,
				BlockFilterDetail::EquationsLevel0T&		equations,
				BlockFilterDetail::MaxDeviationT&			maxTimeDeviation,
				BlockFilterDetail::MaxDeviationsLevel0T&	maxDeviations,
				Position&									interpolation
			);
		virtual ~DetailWriterLevel0();

		void	WriteDetail
			(
				unsigned long	startIndex,
				unsigned long	count,
				long			lineNumber,
				long			disableCount,
				long			maxLoopCount
			);
		void InitTimeStatistic();
		void InitStatistic();
		void CollectTimeStatistic1	(long startIndex, long count);
		void CollectTimeStatistic2	(long startIndex, long count);
		void CollectStatistic1		(long startIndex, long count);

	private:
		void SkipElement(unsigned long count);
};

#endif
