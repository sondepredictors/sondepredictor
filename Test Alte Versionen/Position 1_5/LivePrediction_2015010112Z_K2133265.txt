SondePrediction 1.5 by Asohen, http://AsphaltOhneEnde.de
File from predict.habhub.org:       ./output.csv
Sondemonitor ground track file:     ./groundtrack2015010112Z_K2133265.txt
Live prediction result file (this): ./LivePrediction_2015010112Z_K2133265.txt
Live prediction GPX file:           ./LivePrediction_2015010112Z_K2133265.gpx
Predicted burst height     by predict.habhub.org: 29896m
Predicted landing position by predict.habhub.org: time=9441s, lat/lon=53.1992 9.24739, height=-0.788615m
Detected ground track file sonde type: RS92

LivePrediction:
23582m, -19m/s = 99%, landing in 53 minutes at lat/lon 53.0703 9.45256
22632m, -18m/s = 100%, landing in 51 minutes at lat/lon 53.0689 9.4453
20150m, -16m/s = 110%, landing in 48 minutes at lat/lon 53.0793 9.40741
19430m, -15m/s = 113%, landing in 47 minutes at lat/lon 53.0827 9.38857
17465m, -12m/s = 105%, landing in 46 minutes at lat/lon 53.0839 9.39866
16305m, -11m/s = 107%, landing in 44 minutes at lat/lon 53.0871 9.38543
15759m, -11m/s = 108%, landing in 43 minutes at lat/lon 53.092 9.3832
14262m, -11m/s = 120%, landing in 38 minutes at lat/lon 53.1117 9.32688
13343m, -9m/s = 116%, landing in 37 minutes at lat/lon 53.1083 9.34446
12503m, -8m/s = 108%, landing in 37 minutes at lat/lon 53.1135 9.36443
11700m, -7m/s = 108%, landing in 36 minutes at lat/lon 53.1238 9.36082
10952m, -7m/s = 108%, landing in 34 minutes at lat/lon 53.1178 9.35008
10242m, -6m/s = 106%, landing in 32 minutes at lat/lon 53.1126 9.35327
9558m, -6m/s = 104%, landing in 31 minutes at lat/lon 53.1102 9.36806
9225m, -6m/s = 101%, landing in 30 minutes at lat/lon 53.1084 9.37582
7965m, -5m/s = 100%, landing in 27 minutes at lat/lon 53.1041 9.40151
7365m, -5m/s = 102%, landing in 25 minutes at lat/lon 53.1012 9.41874
7076m, -5m/s = 97%, landing in 25 minutes at lat/lon 53.0979 9.42837
6229m, -4m/s = 94%, landing in 22 minutes at lat/lon 53.0907 9.44907
5687m, -4m/s = 91%, landing in 21 minutes at lat/lon 53.0901 9.45626
4652m, -4m/s = 95%, landing in 17 minutes at lat/lon 53.0985 9.4658
4152m, -4m/s = 97%, landing in 16 minutes at lat/lon 53.0959 9.4654
3907m, -4m/s = 100%, landing in 15 minutes at lat/lon 53.0957 9.46417
3667m, -4m/s = 99%, landing in 14 minutes at lat/lon 53.0951 9.46245
3194m, -4m/s = 96%, landing in 12 minutes at lat/lon 53.0968 9.4533
2505m, -4m/s = 103%, landing in 9 minutes at lat/lon 53.0975 9.44028
2278m, -4m/s = 102%, landing in 8 minutes at lat/lon 53.0982 9.43836
1836m, -3m/s = 100%, landing in 7 minutes at lat/lon 53.0994 9.43284
1621m, -3m/s = 102%, landing in 6 minutes at lat/lon 53.1004 9.42948
985m, -3m/s = 100%, landing in 4 minutes at lat/lon 53.1012 9.42284
775m, -3m/s = 99%, landing in 3 minutes at lat/lon 53.1003 9.42403
368m, -3m/s = 90%, landing in 2 minutes at lat/lon 53.0985 9.43133
0m, -3m/s = 92%, landing in 0 minutes at lat/lon 53.0983 9.42875
