SondePrediction 1.2 by Asohen, http://AsphaltOhneEnde.de
File from predict.habhub.org:       c:/Users/Asohen/temp/Test Position/output.csv
Sondemonitor ground track file:     c:/Users/Asohen/temp/Test Position/groundtrack2015010112Z_K2133265.txt
Live prediction result file (this): c:/Users/Asohen/temp/Test Position/LivePrediction_2015010112Z_K2133265.txt
Live prediction GPX file:           c:/Users/Asohen/temp/Test Position/LivePrediction_2015010112Z_K2133265.gpx
Predicted burst height     by predict.habhub.org: 29896m
Predicted landing position by predict.habhub.org: time=9441s, lat/lon=53.1992 9.24739, height=-0.788615m
LivePrediction:
^ 12251m, 26m/s = 511%, landing in 117 minutes at lat/lon 53.1738 9.29605
^ 12542m, 11m/s = 223%, landing in 116 minutes at lat/lon 53.1758 9.32021
^ 13030m, 10m/s = 195%, landing in 114 minutes at lat/lon 53.1871 9.31405
^ 13616m, 8m/s = 166%, landing in 112 minutes at lat/lon 53.1905 9.30765
^ 13938m, 7m/s = 130%, landing in 112 minutes at lat/lon 53.1881 9.31285
^ 14281m, 6m/s = 113%, landing in 110 minutes at lat/lon 53.1911 9.30155
^ 14592m, 5m/s = 106%, landing in 109 minutes at lat/lon 53.1901 9.29981
^ 15229m, 6m/s = 120%, landing in 108 minutes at lat/lon 53.1878 9.32253
^ 15752m, 6m/s = 114%, landing in 107 minutes at lat/lon 53.1991 9.31599
^ 16127m, 6m/s = 110%, landing in 104 minutes at lat/lon 53.2092 9.28587
^ 16556m, 6m/s = 117%, landing in 103 minutes at lat/lon 53.205 9.29757
^ 16878m, 5m/s = 110%, landing in 102 minutes at lat/lon 53.2071 9.28449
^ 17238m, 6m/s = 117%, landing in 101 minutes at lat/lon 53.2062 9.29745
^ 17615m, 6m/s = 125%, landing in 100 minutes at lat/lon 53.2049 9.30307
^ 17951m, 6m/s = 120%, landing in 98 minutes at lat/lon 53.208 9.28899
^ 18276m, 6m/s = 111%, landing in 97 minutes at lat/lon 53.2106 9.27521
^ 18620m, 6m/s = 125%, landing in 96 minutes at lat/lon 53.2089 9.27895
^ 18965m, 6m/s = 123%, landing in 95 minutes at lat/lon 53.2018 9.27423
^ 19295m, 6m/s = 116%, landing in 93 minutes at lat/lon 53.2066 9.25469
^ 19623m, 6m/s = 112%, landing in 92 minutes at lat/lon 53.2083 9.25123
^ 20054m, 7m/s = 135%, landing in 92 minutes at lat/lon 53.2063 9.25721
^ 20472m, 7m/s = 144%, landing in 90 minutes at lat/lon 53.2034 9.24247
^ 20856m, 7m/s = 130%, landing in 88 minutes at lat/lon 53.2032 9.22875
^ 21171m, 6m/s = 120%, landing in 87 minutes at lat/lon 53.2036 9.22611
^ 21502m, 6m/s = 118%, landing in 87 minutes at lat/lon 53.201 9.23009
^ 21789m, 5m/s = 92%, landing in 86 minutes at lat/lon 53.1929 9.23721
^ 22353m, 8m/s = 157%, landing in 84 minutes at lat/lon 53.1889 9.22803
^ 22768m, 7m/s = 136%, landing in 82 minutes at lat/lon 53.1933 9.21191
^ 23128m, 6m/s = 124%, landing in 81 minutes at lat/lon 53.1928 9.20323
^ 23493m, 6m/s = 121%, landing in 80 minutes at lat/lon 53.1948 9.20097
^ 23854m, 6m/s = 120%, landing in 79 minutes at lat/lon 53.194 9.20663
^ 24203m, 6m/s = 118%, landing in 77 minutes at lat/lon 53.197 9.20123
^ 24559m, 6m/s = 119%, landing in 76 minutes at lat/lon 53.2 9.19631
^ 24924m, 6m/s = 121%, landing in 75 minutes at lat/lon 53.1996 9.19921
^ 25303m, 6m/s = 124%, landing in 74 minutes at lat/lon 53.1993 9.19945
^ 25714m, 7m/s = 131%, landing in 72 minutes at lat/lon 53.2034 9.18691
^ 26082m, 6m/s = 125%, landing in 71 minutes at lat/lon 53.209 9.17051
^ 26461m, 6m/s = 126%, landing in 70 minutes at lat/lon 53.2098 9.16981
^ 26874m, 7m/s = 142%, landing in 69 minutes at lat/lon 53.2072 9.17215
^ 27306m, 7m/s = 145%, landing in 67 minutes at lat/lon 53.2118 9.15621
^ 27733m, 8m/s = 151%, landing in 66 minutes at lat/lon 53.2157 9.13235
^ 28111m, 7m/s = 132%, landing in 64 minutes at lat/lon 53.2234 9.10657
^ 28455m, 6m/s = 128%, landing in 63 minutes at lat/lon 53.2189 9.11403
^ 28841m, 7m/s = 131%, landing in 62 minutes at lat/lon 53.2286 9.09443
^ 29285m, 7m/s = 149%, landing in 61 minutes at lat/lon 53.2281 9.09425
^ 29721m, 7m/s = 146%, landing in 59 minutes at lat/lon 53.2369 9.06785
^ 29896m ,no prediction for this height
^ 29901m ,no prediction for this height
^ 29906m ,no prediction for this height
^ 29904m ,no prediction for this height
^ 29914m ,no prediction for this height
^ 29921m ,no prediction for this height
^ 29928m ,no prediction for this height
^ 29931m ,no prediction for this height
^ 29937m ,no prediction for this height
^ 29938m ,no prediction for this height
^ 29964m ,no prediction for this height
^ 29953m ,no prediction for this height
^ 29963m ,no prediction for this height
^ 29966m ,no prediction for this height
^ 29974m ,no prediction for this height
^ 29979m ,no prediction for this height
^ 29986m ,no prediction for this height
^ 29992m ,no prediction for this height
^ 29990m ,no prediction for this height
^ 30012m ,no prediction for this height
^ 30026m ,no prediction for this height
^ 30025m ,no prediction for this height
^ 30030m ,no prediction for this height
^ 30037m ,no prediction for this height
^ 30037m ,no prediction for this height
^ 30051m ,no prediction for this height
^ 30077m ,no prediction for this height
^ 30093m ,no prediction for this height
^ 30105m ,no prediction for this height
^ 30103m ,no prediction for this height
^ 30103m ,no prediction for this height
^ 30117m ,no prediction for this height
^ 30120m ,no prediction for this height
^ 30138m ,no prediction for this height
^ 30147m ,no prediction for this height
^ 30139m ,no prediction for this height
^ 30156m ,no prediction for this height
^ 30161m ,no prediction for this height
^ 30167m ,no prediction for this height
^ 30165m ,no prediction for this height
^ 30172m ,no prediction for this height
^ 30171m ,no prediction for this height
^ 30189m ,no prediction for this height
^ 30201m ,no prediction for this height
^ 30200m ,no prediction for this height
^ 30195m ,no prediction for this height
^ 30209m ,no prediction for this height
^ 30212m ,no prediction for this height
^ 30219m ,no prediction for this height
^ 30239m ,no prediction for this height
^ 30255m ,no prediction for this height
^ 30255m ,no prediction for this height
^ 30264m ,no prediction for this height
^ 30262m ,no prediction for this height
^ 30264m ,no prediction for this height
^ 30280m ,no prediction for this height
^ 30275m ,no prediction for this height
^ 30287m ,no prediction for this height
^ 30298m ,no prediction for this height
^ 30294m ,no prediction for this height
^ 30296m ,no prediction for this height
^ 30309m ,no prediction for this height
^ 30318m ,no prediction for this height
^ 30332m ,no prediction for this height
^ 30329m ,no prediction for this height
^ 30339m ,no prediction for this height
^ 30354m ,no prediction for this height
^ 30360m ,no prediction for this height
^ 30365m ,no prediction for this height
^ 30364m ,no prediction for this height
^ 30368m ,no prediction for this height
^ 30371m ,no prediction for this height
^ 30397m ,no prediction for this height
^ 30409m ,no prediction for this height
^ 30393m ,no prediction for this height
^ 30418m ,no prediction for this height
^ 30422m ,no prediction for this height
^ 30427m ,no prediction for this height
^ 30434m ,no prediction for this height
^ 30431m ,no prediction for this height
^ 30451m ,no prediction for this height
^ 30451m ,no prediction for this height
^ 30453m ,no prediction for this height
^ 30456m ,no prediction for this height
^ 30459m ,no prediction for this height
^ 30476m ,no prediction for this height
^ 30497m ,no prediction for this height
^ 30493m ,no prediction for this height
^ 30509m ,no prediction for this height
^ 30508m ,no prediction for this height
^ 30524m ,no prediction for this height
^ 30522m ,no prediction for this height
^ 30542m ,no prediction for this height
^ 30544m ,no prediction for this height
^ 30574m ,no prediction for this height
^ 30562m ,no prediction for this height
^ 30567m ,no prediction for this height
^ 30580m ,no prediction for this height
^ 30581m ,no prediction for this height
^ 30594m ,no prediction for this height
^ 30599m ,no prediction for this height
^ 30604m ,no prediction for this height
^ 30608m ,no prediction for this height
^ 30625m ,no prediction for this height
^ 30632m ,no prediction for this height
^ 30640m ,no prediction for this height
^ 30655m ,no prediction for this height
^ 30654m ,no prediction for this height
^ 30655m ,no prediction for this height
^ 30672m ,no prediction for this height
^ 30671m ,no prediction for this height
^ 30683m ,no prediction for this height
^ 30702m ,no prediction for this height
^ 30709m ,no prediction for this height
^ 30723m ,no prediction for this height
^ 30727m ,no prediction for this height
^ 30741m ,no prediction for this height
^ 30753m ,no prediction for this height
^ 30752m ,no prediction for this height
^ 30763m ,no prediction for this height
^ 30782m ,no prediction for this height
^ 30786m ,no prediction for this height
^ 30802m ,no prediction for this height
^ 30806m ,no prediction for this height
^ 30801m ,no prediction for this height
^ 30819m ,no prediction for this height
^ 30821m ,no prediction for this height
^ 30835m ,no prediction for this height
^ 30840m ,no prediction for this height
^ 30841m ,no prediction for this height
^ 30851m ,no prediction for this height
^ 30855m ,no prediction for this height
^ 30866m ,no prediction for this height
^ 30885m ,no prediction for this height
^ 30876m ,no prediction for this height
^ 30897m ,no prediction for this height
^ 30907m ,no prediction for this height
^ 30915m ,no prediction for this height
^ 30921m ,no prediction for this height
^ 30935m ,no prediction for this height
^ 30933m ,no prediction for this height
^ 30951m ,no prediction for this height
^ 30946m ,no prediction for this height
^ 30955m ,no prediction for this height
^ 30984m ,no prediction for this height
^ 30980m ,no prediction for this height
^ 30995m ,no prediction for this height
^ 31000m ,no prediction for this height
^ 31003m ,no prediction for this height
^ 31018m ,no prediction for this height
^ 31023m ,no prediction for this height
^ 31028m ,no prediction for this height
^ 31042m ,no prediction for this height
^ 31047m ,no prediction for this height
^ 31058m ,no prediction for this height
^ 31059m ,no prediction for this height
^ 31063m ,no prediction for this height
^ 31081m ,no prediction for this height
^ 31097m ,no prediction for this height
^ 31107m ,no prediction for this height
^ 31104m ,no prediction for this height
^ 31121m ,no prediction for this height
^ 31134m ,no prediction for this height
^ 31127m ,no prediction for this height
^ 31144m ,no prediction for this height
^ 31147m ,no prediction for this height
^ 31160m ,no prediction for this height
^ 31183m ,no prediction for this height
^ 31161m ,no prediction for this height
^ 31182m ,no prediction for this height
^ 31187m ,no prediction for this height
^ 31191m ,no prediction for this height
^ 31207m ,no prediction for this height
^ 31243m ,no prediction for this height
^ 31235m ,no prediction for this height
^ 31240m ,no prediction for this height
^ 31256m ,no prediction for this height
^ 31244m ,no prediction for this height
^ 31249m ,no prediction for this height
^ 31262m ,no prediction for this height
^ 31270m ,no prediction for this height
^ 31285m ,no prediction for this height
^ 31303m ,no prediction for this height
^ 31296m ,no prediction for this height
^ 31301m ,no prediction for this height
^ 31331m ,no prediction for this height
^ 31321m ,no prediction for this height
^ 31337m ,no prediction for this height
^ 31352m ,no prediction for this height
^ 31357m ,no prediction for this height
^ 31343m ,no prediction for this height
^ 31366m ,no prediction for this height
^ 31377m ,no prediction for this height
^ 31386m ,no prediction for this height
^ 31391m ,no prediction for this height
^ 31387m ,no prediction for this height
^ 31397m ,no prediction for this height
^ 31414m ,no prediction for this height
^ 31432m ,no prediction for this height
^ 31439m ,no prediction for this height
^ 31435m ,no prediction for this height
^ 31437m ,no prediction for this height
^ 31435m ,no prediction for this height
^ 31450m ,no prediction for this height
^ 31468m ,no prediction for this height
^ 31478m ,no prediction for this height
^ 31481m ,no prediction for this height
^ 31499m ,no prediction for this height
^ 31513m ,no prediction for this height
^ 31519m ,no prediction for this height
^ 31527m ,no prediction for this height
^ 31525m ,no prediction for this height
^ 31543m ,no prediction for this height
^ 31543m ,no prediction for this height
^ 31526m ,no prediction for this height
^ 31547m ,no prediction for this height
^ 31576m ,no prediction for this height
^ 31563m ,no prediction for this height
^ 31583m ,no prediction for this height
^ 31593m ,no prediction for this height
^ 31602m ,no prediction for this height
^ 31592m ,no prediction for this height
^ 31615m ,no prediction for this height
^ 31623m ,no prediction for this height
^ 31631m ,no prediction for this height
^ 31641m ,no prediction for this height
^ 31636m ,no prediction for this height
^ 31657m ,no prediction for this height
^ 31644m ,no prediction for this height
^ 31651m ,no prediction for this height
^ 31669m ,no prediction for this height
^ 31658m ,no prediction for this height
^ 31665m ,no prediction for this height
^ 31686m ,no prediction for this height
^ 31693m ,no prediction for this height
^ 31699m ,no prediction for this height
^ 31721m ,no prediction for this height
^ 31720m ,no prediction for this height
^ 31731m ,no prediction for this height
^ 31726m ,no prediction for this height
^ 31745m ,no prediction for this height
^ 31757m ,no prediction for this height
^ 31765m ,no prediction for this height
^ 31772m ,no prediction for this height
^ 31771m ,no prediction for this height
^ 31773m ,no prediction for this height
^ 31781m ,no prediction for this height
^ 31780m ,no prediction for this height
^ 31801m ,no prediction for this height
^ 31800m ,no prediction for this height
^ 31814m ,no prediction for this height
^ 31808m ,no prediction for this height
^ 31817m ,no prediction for this height
^ 31827m ,no prediction for this height
^ 31845m ,no prediction for this height
^ 31838m ,no prediction for this height
^ 31845m ,no prediction for this height
^ 31859m ,no prediction for this height
^ 31864m ,no prediction for this height
^ 31862m ,no prediction for this height
^ 31875m ,no prediction for this height
^ 31873m ,no prediction for this height
^ 31886m ,no prediction for this height
^ 31890m ,no prediction for this height
^ 31902m ,no prediction for this height
^ 31913m ,no prediction for this height
^ 31905m ,no prediction for this height
^ 31934m ,no prediction for this height
^ 31934m ,no prediction for this height
^ 31931m ,no prediction for this height
^ 31937m ,no prediction for this height
^ 31941m ,no prediction for this height
^ 31942m ,no prediction for this height
^ 31962m ,no prediction for this height
^ 31979m ,no prediction for this height
^ 31976m ,no prediction for this height
^ 31957m ,no prediction for this height
^ 31983m ,no prediction for this height
^ 31996m ,no prediction for this height
^ 31991m ,no prediction for this height
^ 32010m ,no prediction for this height
^ 32014m ,no prediction for this height
^ 32024m ,no prediction for this height
^ 32018m ,no prediction for this height
^ 32031m ,no prediction for this height
^ 32044m ,no prediction for this height
^ 32030m ,no prediction for this height
^ 32014m ,no prediction for this height
^ 32071m ,no prediction for this height
^ 32076m ,no prediction for this height
^ 32107m ,no prediction for this height
^ 32107m ,no prediction for this height
^ 32132m ,no prediction for this height
^ 32120m ,no prediction for this height
^ 32123m ,no prediction for this height
^ 32138m ,no prediction for this height
^ 32153m ,no prediction for this height
^ 32162m ,no prediction for this height
^ 32163m ,no prediction for this height
^ 32172m ,no prediction for this height
^ 32183m ,no prediction for this height
^ 32191m ,no prediction for this height
^ 32212m ,no prediction for this height
^ 32215m ,no prediction for this height
^ 32227m ,no prediction for this height
^ 32243m ,no prediction for this height
^ 32235m ,no prediction for this height
^ 32246m ,no prediction for this height
^ 32267m ,no prediction for this height
^ 32265m ,no prediction for this height
^ 32255m ,no prediction for this height
^ 32266m ,no prediction for this height
^ 32266m ,no prediction for this height
^ 32275m ,no prediction for this height
^ 32290m ,no prediction for this height
^ 32299m ,no prediction for this height
^ 32305m ,no prediction for this height
^ 32304m ,no prediction for this height
^ 32330m ,no prediction for this height
^ 32322m ,no prediction for this height
^ 32353m ,no prediction for this height
^ 32342m ,no prediction for this height
^ 32353m ,no prediction for this height
^ 32370m ,no prediction for this height
^ 32396m ,no prediction for this height
^ 32368m ,no prediction for this height
^ 32384m ,no prediction for this height
^ 32388m ,no prediction for this height
^ 32385m ,no prediction for this height
^ 32404m ,no prediction for this height
^ 32394m ,no prediction for this height
^ 32416m ,no prediction for this height
^ 32416m ,no prediction for this height
^ 32423m ,no prediction for this height
^ 32468m ,no prediction for this height
^ 32480m ,no prediction for this height
^ 32512m ,no prediction for this height
^ 32493m ,no prediction for this height
^ 32495m ,no prediction for this height
^ 32503m ,no prediction for this height
^ 32537m ,no prediction for this height
^ 32528m ,no prediction for this height
^ 32528m ,no prediction for this height
^ 32540m ,no prediction for this height
^ 32552m ,no prediction for this height
^ 32556m ,no prediction for this height
^ 32567m ,no prediction for this height
^ 32565m ,no prediction for this height
^ 32577m ,no prediction for this height
^ 32589m ,no prediction for this height
^ 32601m ,no prediction for this height
^ 32585m ,no prediction for this height
^ 32578m ,no prediction for this height
^ 32604m ,no prediction for this height
^ 32618m ,no prediction for this height
^ 32607m ,no prediction for this height
^ 32618m ,no prediction for this height
^ 32617m ,no prediction for this height
^ 32649m ,no prediction for this height
^ 32658m ,no prediction for this height
^ 32675m ,no prediction for this height
^ 32683m ,no prediction for this height
^ 32678m ,no prediction for this height
^ 32664m ,no prediction for this height
^ 32684m ,no prediction for this height
^ 32709m ,no prediction for this height
^ 32710m ,no prediction for this height
^ 32724m ,no prediction for this height
^ 32720m ,no prediction for this height
^ 32732m ,no prediction for this height
^ 32707m ,no prediction for this height
^ 32726m ,no prediction for this height
^ 32747m ,no prediction for this height
^ 32763m ,no prediction for this height
^ 32752m ,no prediction for this height
^ 32771m ,no prediction for this height
^ 32780m ,no prediction for this height
^ 32789m ,no prediction for this height
^ 32796m ,no prediction for this height
^ 32795m ,no prediction for this height
^ 32807m ,no prediction for this height
^ 32816m ,no prediction for this height
^ 32838m ,no prediction for this height
^ 32815m ,no prediction for this height
^ 32855m ,no prediction for this height
^ 32841m ,no prediction for this height
^ 32856m ,no prediction for this height
^ 32877m ,no prediction for this height
^ 32875m ,no prediction for this height
^ 32881m ,no prediction for this height
^ 32886m ,no prediction for this height
^ 32882m ,no prediction for this height
^ 32879m ,no prediction for this height
^ 32916m ,no prediction for this height
^ 32939m ,no prediction for this height
^ 32934m ,no prediction for this height
^ 32944m ,no prediction for this height
^ 32934m ,no prediction for this height
^ 32937m ,no prediction for this height
^ 32981m ,no prediction for this height
^ 32989m ,no prediction for this height
^ 32993m ,no prediction for this height
^ 32988m ,no prediction for this height
^ 32976m ,no prediction for this height
^ 32996m ,no prediction for this height
^ 32992m ,no prediction for this height
^ 33020m ,no prediction for this height
^ 33029m ,no prediction for this height
^ 33025m ,no prediction for this height
^ 33016m ,no prediction for this height
^ 33051m ,no prediction for this height
^ 33023m ,no prediction for this height
^ 33046m ,no prediction for this height
^ 33048m ,no prediction for this height
^ 33069m ,no prediction for this height
^ 33068m ,no prediction for this height
^ 33086m ,no prediction for this height
^ 33064m ,no prediction for this height
^ 33104m ,no prediction for this height
^ 33124m ,no prediction for this height
^ 33117m ,no prediction for this height
^ 33115m ,no prediction for this height
^ 33129m ,no prediction for this height
^ 33140m ,no prediction for this height
^ 33152m ,no prediction for this height
^ 33163m ,no prediction for this height
^ 33152m ,no prediction for this height
^ 33155m ,no prediction for this height
^ 33181m ,no prediction for this height
^ 33198m ,no prediction for this height
^ 33194m ,no prediction for this height
^ 33205m ,no prediction for this height
^ 33197m ,no prediction for this height
^ 33209m ,no prediction for this height
^ 33218m ,no prediction for this height
^ 33219m ,no prediction for this height
^ 33231m ,no prediction for this height
^ 33236m ,no prediction for this height
^ 33270m ,no prediction for this height
^ 33292m ,no prediction for this height
^ 33284m ,no prediction for this height
^ 33279m ,no prediction for this height
^ 33283m ,no prediction for this height
^ 33310m ,no prediction for this height
^ 33310m ,no prediction for this height
^ 33308m ,no prediction for this height
^ 33349m ,no prediction for this height
^ 33348m ,no prediction for this height
^ 33368m ,no prediction for this height
^ 33350m ,no prediction for this height
^ 33370m ,no prediction for this height
^ 33383m ,no prediction for this height
^ 33368m ,no prediction for this height
^ 33389m ,no prediction for this height
^ 33382m ,no prediction for this height
^ 33404m ,no prediction for this height
^ 33411m ,no prediction for this height
^ 33407m ,no prediction for this height
^ 33414m ,no prediction for this height
^ 33448m ,no prediction for this height
^ 33450m ,no prediction for this height
^ 33468m ,no prediction for this height
^ 33449m ,no prediction for this height
^ 33474m ,no prediction for this height
^ 33477m ,no prediction for this height
^ 33479m ,no prediction for this height
^ 33480m ,no prediction for this height
^ 33496m ,no prediction for this height
^ 33499m ,no prediction for this height
^ 33505m ,no prediction for this height
^ 33517m ,no prediction for this height
^ 33509m ,no prediction for this height
^ 33531m ,no prediction for this height
^ 33543m ,no prediction for this height
^ 33543m ,no prediction for this height
^ 33546m ,no prediction for this height
^ 33540m ,no prediction for this height
^ 33533m ,no prediction for this height
^ 33562m ,no prediction for this height
^ 33563m ,no prediction for this height
^ 33572m ,no prediction for this height
^ 33579m ,no prediction for this height
^ 33603m ,no prediction for this height
^ 33604m ,no prediction for this height
^ 33619m ,no prediction for this height
^ 33593m ,no prediction for this height
^ 33622m ,no prediction for this height
^ 33624m ,no prediction for this height
^ 33616m ,no prediction for this height
^ 33641m ,no prediction for this height
^ 33633m ,no prediction for this height
^ 33655m ,no prediction for this height
^ 33653m ,no prediction for this height
^ 33656m ,no prediction for this height
^ 33657m ,no prediction for this height
^ 33684m ,no prediction for this height
^ 33687m ,no prediction for this height
^ 33697m ,no prediction for this height
^ 33702m ,no prediction for this height
^ 33702m ,no prediction for this height
^ 33705m ,no prediction for this height
^ 33743m ,no prediction for this height
^ 33721m ,no prediction for this height
^ 33735m ,no prediction for this height
^ 33740m ,no prediction for this height
^ 33775m ,no prediction for this height
^ 33782m ,no prediction for this height
^ 33756m ,no prediction for this height
^ 33769m ,no prediction for this height
^ 33780m ,no prediction for this height
^ 33755m ,no prediction for this height
^ 33770m ,no prediction for this height
^ 33758m ,no prediction for this height
^ 33771m ,no prediction for this height
^ 33782m ,no prediction for this height
^ 33794m ,no prediction for this height
^ 33844m ,no prediction for this height
^ 33830m ,no prediction for this height
^ 33818m ,no prediction for this height
^ 33835m ,no prediction for this height
^ 33876m ,no prediction for this height
^ 33857m ,no prediction for this height
^ 33861m ,no prediction for this height
^ 33855m ,no prediction for this height
^ 33881m ,no prediction for this height
^ 33890m ,no prediction for this height
^ 33893m ,no prediction for this height
^ 33895m ,no prediction for this height
^ 33895m ,no prediction for this height
^ 33934m ,no prediction for this height
^ 33928m ,no prediction for this height
^ 33911m ,no prediction for this height
^ 33931m ,no prediction for this height
^ 33961m ,no prediction for this height
^ 33938m ,no prediction for this height
^ 33943m ,no prediction for this height
^ 33956m ,no prediction for this height
^ 33937m ,no prediction for this height
^ 33954m ,no prediction for this height
^ 33934m ,no prediction for this height
^ 33884m ,no prediction for this height
^ 33862m ,no prediction for this height
^ 33780m ,no prediction for this height
^ 33739m ,no prediction for this height
! 33704m ,no prediction for this height
! 33669m ,no prediction for this height
! 33630m ,no prediction for this height
! 33566m ,no prediction for this height
! 33518m ,no prediction for this height
! 33481m ,no prediction for this height
! 33432m ,no prediction for this height
! 33365m ,no prediction for this height
! 33273m ,no prediction for this height
! 33214m ,no prediction for this height
! 33162m ,no prediction for this height
! 32869m ,no prediction for this height
! 32887m ,no prediction for this height
! 32859m ,no prediction for this height
! 32795m ,no prediction for this height
! 32761m ,no prediction for this height
! 32703m ,no prediction for this height
! 32674m ,no prediction for this height
! 32636m ,no prediction for this height
! 32635m ,no prediction for this height
! 32579m ,no prediction for this height
! 32535m ,no prediction for this height
! 32459m ,no prediction for this height
! 32442m ,no prediction for this height
! 32418m ,no prediction for this height
! 32345m ,no prediction for this height
! 32297m ,no prediction for this height
! 32222m ,no prediction for this height
! 32184m ,no prediction for this height
! 32147m ,no prediction for this height
! 32097m ,no prediction for this height
! 32070m ,no prediction for this height
! 31981m ,no prediction for this height
! 31950m ,no prediction for this height
! 31910m ,no prediction for this height
! 31872m ,no prediction for this height
! 31816m ,no prediction for this height
! 31810m ,no prediction for this height
! 31796m ,no prediction for this height
! 31757m ,no prediction for this height
! 31707m ,no prediction for this height
! 31660m ,no prediction for this height
! 31601m ,no prediction for this height
! 31580m ,no prediction for this height
! 31540m ,no prediction for this height
! 31524m ,no prediction for this height
! 31500m ,no prediction for this height
! 31449m ,no prediction for this height
! 31391m ,no prediction for this height
! 31376m ,no prediction for this height
! 31317m ,no prediction for this height
! 31287m ,no prediction for this height
! 31280m ,no prediction for this height
! 31231m ,no prediction for this height
! 31167m ,no prediction for this height
! 31145m ,no prediction for this height
! 31086m ,no prediction for this height
! 31057m ,no prediction for this height
! 30996m ,no prediction for this height
! 30950m ,no prediction for this height
! 30929m ,no prediction for this height
! 30863m ,no prediction for this height
! 30836m ,no prediction for this height
! 30811m ,no prediction for this height
! 30793m ,no prediction for this height
! 30745m ,no prediction for this height
! 30684m ,no prediction for this height
! 30644m ,no prediction for this height
! 30612m ,no prediction for this height
! 30570m ,no prediction for this height
! 30534m ,no prediction for this height
! 30515m ,no prediction for this height
! 30496m ,no prediction for this height
! 30447m ,no prediction for this height
! 30418m ,no prediction for this height
! 30381m ,no prediction for this height
! 30349m ,no prediction for this height
! 30339m ,no prediction for this height
! 30335m ,no prediction for this height
! 30274m ,no prediction for this height
! 30224m ,no prediction for this height
! 30202m ,no prediction for this height
! 30146m ,no prediction for this height
! 30122m ,no prediction for this height
! 30105m ,no prediction for this height
! 30059m ,no prediction for this height
! 30001m ,no prediction for this height
! 29969m ,no prediction for this height
! 29944m ,no prediction for this height
! 29907m ,no prediction for this height
! 29879m, -30m/s landing in 58 minutes at lat/lon 53.037 9.50318
! 27946m, -30m/s = 98%, landing in 57 minutes at lat/lon 53.0591 9.46946
! 26241m, -27m/s = 102%, landing in 56 minutes at lat/lon 53.0581 9.46571
! 24750m, -25m/s = 105%, landing in 55 minutes at lat/lon 53.0614 9.45867
! 23585m, -20m/s = 103%, landing in 53 minutes at lat/lon 53.0703 9.45258
! 22475m, -18m/s = 99%, landing in 52 minutes at lat/lon 53.0691 9.44771
! 21387m, -17m/s = 103%, landing in 52 minutes at lat/lon 53.0699 9.44139
! 20297m, -17m/s = 110%, landing in 51 minutes at lat/lon 53.0692 9.43487
! 19290m, -16m/s = 117%, landing in 49 minutes at lat/lon 53.0745 9.42059
! 18297m, -15m/s = 115%, landing in 48 minutes at lat/lon 53.0775 9.41509
! 17527m, -13m/s = 105%, landing in 47 minutes at lat/lon 53.0793 9.41327
! 16783m, -12m/s = 104%, landing in 47 minutes at lat/lon 53.0795 9.41415
! 16043m, -11m/s = 105%, landing in 45 minutes at lat/lon 53.0848 9.40195
! 15227m, -10m/s = 103%, landing in 44 minutes at lat/lon 53.0867 9.40795
! 14541m, -10m/s = 108%, landing in 42 minutes at lat/lon 53.0924 9.39542
! 13896m, -10m/s = 110%, landing in 42 minutes at lat/lon 53.0944 9.3989
! 13261m, -9m/s = 114%, landing in 40 minutes at lat/lon 53.0986 9.38561
! 12705m, -9m/s = 110%, landing in 39 minutes at lat/lon 53.1001 9.38512
! 12191m, -8m/s = 105%, landing in 38 minutes at lat/lon 53.1034 9.39096
! 11696m, -7m/s = 106%, landing in 37 minutes at lat/lon 53.1219 9.36967
! 11224m, -7m/s = 106%, landing in 36 minutes at lat/lon 53.1163 9.36692
! 10742m, -7m/s = 108%, landing in 35 minutes at lat/lon 53.1066 9.36705
! 10302m, -6m/s = 104%, landing in 34 minutes at lat/lon 53.1008 9.37166
! 9853m, -6m/s = 104%, landing in 33 minutes at lat/lon 53.0968 9.38363
! 9441m, -6m/s = 103%, landing in 32 minutes at lat/lon 53.1063 9.37823
! 9049m, -6m/s = 101%, landing in 31 minutes at lat/lon 53.1026 9.38745
! 8639m, -5m/s = 100%, landing in 30 minutes at lat/lon 53.1031 9.39647
! 8272m, -5m/s = 98%, landing in 29 minutes at lat/lon 53.0992 9.40694
! 7909m, -5m/s = 100%, landing in 27 minutes at lat/lon 53.1032 9.40564
! 7537m, -5m/s = 102%, landing in 27 minutes at lat/lon 53.101 9.41936
! 7199m, -5m/s = 97%, landing in 26 minutes at lat/lon 53.0982 9.43189
! 6873m, -4m/s = 94%, landing in 25 minutes at lat/lon 53.0948 9.4437
! 6560m, -4m/s = 91%, landing in 24 minutes at lat/lon 53.0923 9.45514
! 6242m, -4m/s = 82%, landing in 23 minutes at lat/lon 53.0899 9.46401
! 5946m, -4m/s = 87%, landing in 22 minutes at lat/lon 53.0884 9.47104
! 5658m, -4m/s = 84%, landing in 21 minutes at lat/lon 53.09 9.45876
! 5380m, -4m/s = 87%, landing in 20 minutes at lat/lon 53.0921 9.46329
! 5087m, -4m/s = 93%, landing in 19 minutes at lat/lon 53.0956 9.46719
! 4790m, -4m/s = 95%, landing in 18 minutes at lat/lon 53.0983 9.47112
! 4506m, -4m/s = 94%, landing in 17 minutes at lat/lon 53.0976 9.47384
! 4226m, -4m/s = 94%, landing in 17 minutes at lat/lon 53.0962 9.47623
! 3925m, -4m/s = 100%, landing in 16 minutes at lat/lon 53.0959 9.47802
! 3640m, -4m/s = 95%, landing in 14 minutes at lat/lon 53.0949 9.46343
! 3367m, -4m/s = 96%, landing in 13 minutes at lat/lon 53.0971 9.46204
! 3092m, -4m/s = 96%, landing in 12 minutes at lat/lon 53.0973 9.45633
! 2815m, -4m/s = 99%, landing in 12 minutes at lat/lon 53.0981 9.45151
! 2533m, -4m/s = 102%, landing in 11 minutes at lat/lon 53.0985 9.45296
! 2260m, -4m/s = 101%, landing in 10 minutes at lat/lon 53.0997 9.45277
! 1994m, -4m/s = 102%, landing in 8 minutes at lat/lon 53.0991 9.43867
! 1729m, -3m/s = 101%, landing in 7 minutes at lat/lon 53.1008 9.43769
! 1467m, -3m/s = 101%, landing in 7 minutes at lat/lon 53.1016 9.43676
! 1214m, -3m/s = 100%, landing in 6 minutes at lat/lon 53.1024 9.43646
! 964m, -3m/s = 100%, landing in 4 minutes at lat/lon 53.1011 9.42422
! 723m, -3m/s = 92%, landing in 3 minutes at lat/lon 53.1005 9.4283
! 508m, -3m/s = 86%, landing in 2 minutes at lat/lon 53.0996 9.43413
! 290m, -3m/s = 89%, landing in 2 minutes at lat/lon 53.1002 9.43527
! 52m, -3m/s = 93%, landing in 1 minutes at lat/lon 53.1008 9.43465
! 0m, -1m/s = 52%, landing in 0 minutes at lat/lon 53.0995 9.43101
! 0m, 0m/s landing in 0 minutes at lat/lon 53.0997 9.43132
! 0m, 0m/s landing in 0 minutes at lat/lon 53.0995 9.43101
! 0m, 0m/s landing in 0 minutes at lat/lon 53.1021 9.43225
! 0m, 0m/s landing in 0 minutes at lat/lon 53.0997 9.43112
! 0m, 0m/s landing in 0 minutes at lat/lon 53.0996 9.43091
! 0m, 0m/s landing in 0 minutes at lat/lon 53.0996 9.43101
! 0m, 0m/s landing in 0 minutes at lat/lon 53.0996 9.43081
